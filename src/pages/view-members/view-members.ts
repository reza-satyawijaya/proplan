import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController, NavParams } from 'ionic-angular';
import { NetworkService } from '../../services/network';
import { AccountService } from '../../services/account';
import { Http, Headers, Response }      from '@angular/http';
/**
 * Generated class for the ViewMembersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-members',
  templateUrl: 'view-members.html',
})
export class ViewMembersPage {

  projectId:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,
    private networkService: NetworkService,
    private accountService: AccountService,
    public http:Http) {
    this.projectId = this.navParams.data;
    console.log("projectidmember");
    console.log(this.projectId);
    this.memberData(this.projectId).then((data)=>{
      console.log("Berhasil ambil member");
      console.log(data);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewMembersPage');
  }

  dismissModal(param: any){
    this.viewCtrl.dismiss(param);
  }

  memberData(project):Promise<any>{
    let headers = new Headers();
    let body =
    { 
        sessionToken: this.accountService.getSessionToken(),
        projectId:project
        //sessionToken:"CNcetBkO7YyKak6rQDjuzkkUt3cFM1qEQhRTP0CijrnNquU3VB44iSKdm7BXmcK2juEqZSCbUHfbANRVa8MTjrI48VWiYm61SqJXsxwSqNQbL7QwTIXBzvVHRvBr1ApobzbvClY8ob8vYrz9gEv20jkR6G0AtKffD0yOGKpTVHMRj5JCTB4V1m1uIYGI1u4iVcN1aZDaiA9X22JCSXmp07P5kr09QToqN4oVYuIl5Fx6d0jWH8Z5orlGZgI01OK"
    };
    console.log(body);
    console.log(JSON.stringify(body));

    headers.append('Content-Type','application/json'); 
    return new Promise(resolve =>
    {
      this .http
        .post(this.networkService.getIp()+"/api/project/retrieveMember", body,{headers:headers})
        .subscribe((data) => {
            console.log(data.json());
            if(data.json().status_message){
                  resolve(data.json());
            }else{
                  resolve(data.json());
            }
        })
    });
  }
}
