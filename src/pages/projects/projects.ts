import { Component } from '@angular/core';
import { IonicPage, ModalController, ToastController, ActionSheetController, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import {Http, Response, HttpModule, RequestOptions, Headers} from '@angular/http';

//Pages
import { ProjectPage } from '../project/project';
import { ViewMembersPage } from '../view-members/view-members';

//Services
import { AccountService } from "../../services/account";
import { NetworkService } from "../../services/network";

//Interfaces
import { T_Project } from '../../data/project.interface';
import { T_List } from "../../data/list.interface";
import { T_Task } from "../../data/task.interface";
import { T_User } from '../../data/user.interface';

/**
 * Generated class for the ProjectsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-projects',
    templateUrl: 'projects.html',
})
export class ProjectsPage {
    projectPage: any;
    viewMembersPage: any;
    owner: any;
    loadProgress: any;
    projectList: T_Project[];
    teamId:any = 0;
    constructor(public navCtrl: NavController, 
                public toastCtrl: ToastController, 
                public navParams: NavParams, 
                public alertCtrl: AlertController,
                public modalCtrl: ModalController,
                public actionSheetCtrl: ActionSheetController,
                public http: Http,
                private storage: Storage,
                public loadingCtrl: LoadingController,
                public accountService: AccountService,
                public networkService: NetworkService) {
        this.viewMembersPage = ViewMembersPage;
        this.projectPage = ProjectPage;
        this.teamId = this.navParams.data;
        console.log("teamdeh");
        console.log(this.teamId);
        //retrieve current user name
        this.owner = this.accountService.getName();
        // this.owner = this.navParams.data.owner; //the owner's name, User or Team
        this.projectList = new Array<T_Project>();

        /*let loading = this.loadingCtrl.create({
            content: 'Fetching data...'
        });
        loading.present();
        let response = this.http.post(this.networkService.getIp() + '/api/project/retrieve', {
            'sessionToken': this.accountService.getSessionToken()
        }).subscribe(response => {
            let res = JSON.parse(response.text());
            this.projectList = [];
            for (let project of res.result) {
                let t_project = new T_Project(project.id, project.name, project.userOwnerId, project.userOwnerName, project.star, new Array<T_List>());
                this.projectList.push(t_project);
            }
            this.projectList.sort((leftSide, rightSide): number => {
                if(leftSide.getStar() > rightSide.getStar()) return -1;
                if(leftSide.getStar() < rightSide.getStar()) return 1;
                return 0;
            });
            loading.dismiss();
        });*/
    }

    reloadProjects () {
        let loading = this.loadingCtrl.create({
            content: 'Fetching data...'
        });
        loading.present();
        if(this.teamId == 0){
            let response = this.http.post(this.networkService.getIp() + '/api/project/retrieve', {
                'sessionToken': this.accountService.getSessionToken()
            }).subscribe(response => {
                let res = JSON.parse(response.text());
                this.projectList = [];
                for (let project of res.result) {
                    let t_project = new T_Project(project.id, project.name, project.userOwnerId, project.userOwnerName, project.star, new Array<T_List>());
                    this.projectList.push(t_project);
                }
                this.projectList.sort((leftSide, rightSide): number => {
                    if(leftSide.getStar() > rightSide.getStar()) return -1;
                    if(leftSide.getStar() < rightSide.getStar()) return 1;
                    return 0;
                });
                loading.dismiss();
            });
        }
        else{
            let response = this.http.post(this.networkService.getIp() + '/api/project/retrieveTeam', {
                'sessionToken': this.accountService.getSessionToken(),
                'teamId' : this.teamId
            }).subscribe(response => {
                console.log("Akses project team");
                let res = JSON.parse(response.text());
                this.projectList = [];
                for (let project of res.result) {
                    let t_project = new T_Project(project.id, project.name, project.userOwnerId, project.userOwnerName, project.star, new Array<T_List>());
                    this.projectList.push(t_project);
                }
                this.projectList.sort((leftSide, rightSide): number => {
                    if(leftSide.getStar() > rightSide.getStar()) return -1;
                    if(leftSide.getStar() < rightSide.getStar()) return 1;
                    return 0;
                });
                loading.dismiss();
            }); 
        }
        
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad ProjectsPage');
        this.reloadProjects();
    }

    addProject(){
        console.log("Add Project");
        const alert = this.alertCtrl.create({
            title: 'Add New Project',
            inputs: [
                {name: 'name', placeholder: 'Project Name'}
            ],
            buttons: [
                {
                    text: 'Create',
                    handler: data => {
                        if(data.name.trim() != '' && data.name != null) {
                            let loading = this.loadingCtrl.create({
                                content: 'Please wait...'
                            });
                            loading.present();

                            this.http.post(this.networkService.getIp() + '/api/project/create', {
                                'sessionToken': this.accountService.getSessionToken(),
                                'name': data.name,
                                'userId': this.accountService.getId()
                            }).subscribe(response => {
                                let res = JSON.parse(response.text());
                                if(res.status_code == 600) {
                                    var newProject = new T_Project(res.result, data.name, this.accountService.getId(), this.owner, false, new Array<T_List>());

                                    this.projectList.push(newProject);

                                    let toast = this.toastCtrl.create({
                                        message: "Project created",
                                        duration: 2000,
                                        position: "bottom"
                                    });
                                    toast.present();
                                }
                                loading.dismiss();
                            });
                        } else {
                            let toast = this.toastCtrl.create({
                                message: "Please provide project's name first",
                                duration: 2000,
                                position: "bottom"
                            });
                            toast.present();
                            return false;
                        }
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: data => {
                
                    }
                }
            ]
        });
        alert.present();
        alert.onDidDismiss(() => {
            //oke
        });
    }

    addProjectTeam(){
        console.log("Add Project team");
        const alert = this.alertCtrl.create({
            title: 'Add New Project',
            inputs: [
                {name: 'name', placeholder: 'Project Name'}
            ],
            buttons: [
                {
                    text: 'Create',
                    handler: data => {
                        if(data.name.trim() != '' && data.name != null) {
                            let loading = this.loadingCtrl.create({
                                content: 'Please wait...'
                            });
                            loading.present();

                            this.http.post(this.networkService.getIp() + '/api/project/create', {
                                'sessionToken': this.accountService.getSessionToken(),
                                'name': data.name,
                                'teamId': this.teamId
                            }).subscribe(response => {
                                let res = JSON.parse(response.text());
                                if(res.status_code == 600) {
                                    var newProject = new T_Project(res.result, data.name, this.teamId, this.owner, false, new Array<T_List>());

                                    this.projectList.push(newProject);

                                    let toast = this.toastCtrl.create({
                                        message: "Project created",
                                        duration: 2000,
                                        position: "bottom"
                                    });
                                    toast.present();
                                }
                                loading.dismiss();
                            });
                        } else {
                            let toast = this.toastCtrl.create({
                                message: "Please provide project's name first",
                                duration: 2000,
                                position: "bottom"
                            });
                            toast.present();
                            return false;
                        }
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: data => {
                
                    }
                }
            ]
        });
        alert.present();
        alert.onDidDismiss(() => {
            //oke
        });
    }

    updateProject(p: any){
        const alert = this.alertCtrl.create({
            title: 'Edit ' + p.getName() + "'s name",
            inputs: [
                {name: 'name', placeholder: 'New Project Name'}
            ],
            buttons: [
                {
                    text: 'Edit',
                    handler: data => {
                        if(data.name.trim() != '' && data.name != null) {
                            let loading = this.loadingCtrl.create({
                                content: 'Please wait...'
                            });
                            loading.present();
                            this.http.post(this.networkService.getIp() + '/api/project/update', {
                                'sessionToken': this.accountService.getSessionToken(),
                                'id': p.getId(),
                                'name': data.name
                            }).subscribe(response => {
                                let res = JSON.parse(response.text());
                                if(res.status_code == 600) {
                                    let idx = this.projectList.indexOf(p);
                                    this.projectList[idx].setName(data.name);
                                    let toast = this.toastCtrl.create({
                                        message: "Project name edited",
                                        duration: 2000,
                                        position: "bottom"
                                    });
                                    toast.present();
                                }
                                loading.dismiss();
                            });
                        } else {
                            let toast = this.toastCtrl.create({
                                message: "Please provide project's name first",
                                duration: 2000,
                                position: "bottom"
                            });
                            toast.present();
                            return false;
                        }
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: data => {
                
                    }
                }
            ]
        });
        alert.present();
        alert.onDidDismiss(() => {
            //oke
        });
    }

    //blom dibuat api-nya
    deleteProject(p: any){
        let alert = this.alertCtrl.create({
            title: 'Delete ' + p.name + '?',
            message: 'Are you sure you want to delete this project?',
            buttons: [
                {
                    text: 'Yes',
                    handler: () => {
                        let loading = this.loadingCtrl.create({
                            content: 'Please wait...'
                        });
                        loading.present();
                        this.http.post(this.networkService.getIp() + '/api/project/delete', {
                            'sessionToken': this.accountService.getSessionToken(),
                            'id': p.getId()
                        }).subscribe(response => {
                            let res = JSON.parse(response.text());
                            if(res.status_code == 600) {
                                let idx = this.projectList.indexOf(p);
                                if (idx > -1){
                                    this.projectList.splice(idx, 1);
                                }
                                let toast = this.toastCtrl.create({
                                    message: "Project deleted",
                                    duration: 2000,
                                    position: "bottom"
                                });
                                toast.present();
                            }
                            loading.dismiss();
                        });
                    }
                },
                {
                    text: 'No',
                    handler: () => {

                    }
                }
            ]
        });
        alert.present();
    }


    moveToProject(p: any){
        this.navCtrl.push(this.projectPage,{
            project: p //go to project page carrying the whole information. probably only needs the project ID and query it there
        });
    }
  editProject(p: any){
    let actionSheet = this.actionSheetCtrl.create({
        title: 'Edit ' + p.getName(),
        buttons: [
            {
              text: 'Change Project Name',
              handler: () => {
                this.updateProject(p);
              }
            },
          {
            text: 'Delete Project',
            role: 'destructive',
            handler: () => {
              this.deleteProject(p);
            }
          },
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              
            }
          }
        ]
      });
   
      actionSheet.present();
  }
  isStarred(p: any){
      return p.getStar();
  }
  starProject(p: any){
    let loading = this.loadingCtrl.create({
        content: 'Please wait...'
    });
    //loading.present();
    this.http.post(this.networkService.getIp() + '/api/star/project', {
        'sessionToken': this.accountService.getSessionToken(),
        'projectId': p.getId(),
        'userId': this.accountService.getId()
    }).subscribe(response => {
        let res = JSON.parse(response.text());
        console.log(res);
        if(res.status_code == 600) {
            let idx = this.projectList.indexOf(p);
            if(this.projectList[idx].getStar() == true){
                this.projectList[idx].setStar(false);
                let toast = this.toastCtrl.create({
                    message: "Unstarred " + p.getName(),
                    duration: 2000,
                    position: "bottom"
                });
                toast.present();
            }
            else{
                this.projectList[idx].setStar(true);
                let toast = this.toastCtrl.create({
                    message: "Starred " + p.getName(),
                    duration: 2000,
                    position: "bottom"
                });
                toast.present();
            }
            this.projectList.sort((leftSide, rightSide): number => {
                if(leftSide.getStar() > rightSide.getStar()) return -1;
                if(leftSide.getStar() < rightSide.getStar()) return 1;
                return 0;
            });
        }
        loading.dismiss();
    });
  }
  subscribeProject(p: any){
    let toast = this.toastCtrl.create({
      message: "subscribe to " + p.name + "? (TEMBAK API)",
      duration: 2000,
      position: "bottom"
    });
    toast.present();
  }
}
