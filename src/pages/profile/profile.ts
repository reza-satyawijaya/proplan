import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Http, Headers, Response }      from '@angular/http';
import { ImageResizer, ImageResizerOptions } from '@ionic-native/image-resizer';
import { Crop } from '@ionic-native/crop';
import { Base64 } from '@ionic-native/base64';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ImagePicker } from '@ionic-native/image-picker';
import { NetworkService } from '../../services/network';
import { AccountService } from '../../services/account';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { DashboardPage } from '../../pages/dashboard/dashboard';

import { DomSanitizer } from '@angular/platform-browser';
/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  private profile : FormGroup;
  private dataDiri:any ={};
  private image : any;
  dashboardPage:any = DashboardPage;
  constructor(public navCtrl        : NavController, 
              public navParams      : NavParams, 
              public formBuilder    : FormBuilder, 
              public http           : Http, 
              public alertCtrl      : AlertController,
              private camera        : Camera,
              private imagepick     : ImagePicker,
              private imageResizer  : ImageResizer,
              private crop          : Crop,
              private base64        : Base64,
              private networkService: NetworkService,
              private accountService: AccountService,
              private loadingCtrl   : LoadingController,
              private dom           : DomSanitizer) {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: `Please Wait`
    });
    loading.present();
    this.userData().then((data)=>{
      console.log(data);
      if(data["result"]["profilePicture"] == null) this.dataDiri["profilePicture"] = "assets/imgs/img-placeholder.jpg";
      else this.dataDiri["profilePicture"] = data["result"]["profilePicture"];
      this.dataDiri["fullname"] = data["result"]["fullname"];
      this.dataDiri["username"] = data["result"]["username"];
      this.dataDiri["email"] = data["result"]["email"];
      this.dataDiri["description"] = data["result"]["description"];
      console.log(this.dataDiri);
      this.profile = this.formBuilder.group({
        name: [this.dataDiri["fullname"], Validators.required],
        username: [this.dataDiri["username"], Validators.required],
        email: [this.dataDiri["email"], Validators.required],
        description: [this.dataDiri["description"]],
      });
      //this.image="https://pm1.narvii.com/6411/3bd9805238c8268113654c09aaf4355ffc97a971_hq.jpg";
      this.image=this.dataDiri["profilePicture"];
      loading.dismiss();
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

  userData():Promise<any>{
    let headers = new Headers();
    let body =
    { 
        sessionToken: this.accountService.getSessionToken()
        //sessionToken:"CNcetBkO7YyKak6rQDjuzkkUt3cFM1qEQhRTP0CijrnNquU3VB44iSKdm7BXmcK2juEqZSCbUHfbANRVa8MTjrI48VWiYm61SqJXsxwSqNQbL7QwTIXBzvVHRvBr1ApobzbvClY8ob8vYrz9gEv20jkR6G0AtKffD0yOGKpTVHMRj5JCTB4V1m1uIYGI1u4iVcN1aZDaiA9X22JCSXmp07P5kr09QToqN4oVYuIl5Fx6d0jWH8Z5orlGZgI01OK"
    };
    console.log(body);
    console.log(JSON.stringify(body));

    headers.append('Content-Type','application/json'); 
    return new Promise(resolve =>
    {
      this .http
        .post(this.networkService.getIp()+"/api/user/get", body,{headers:headers})
        .subscribe((data) => {
            console.log(data.json());
            if(data.json().status_message){
                  resolve(data.json());
            }else{
                  resolve(data.json());
            }
        })
    });
  }

  saveForm(){
    this.base64.encodeFile(this.image)
    .then(data_image => 
    {
        this.userSaveData(this.profile.value.name,this.profile.value.username,this.profile.value.description,data_image).then(()=>{
        console.log("Success");
        this.navCtrl.setRoot(this.dashboardPage);
        });
    })
    .catch(err => console.log(err));
  }

  foto()
  {
      const options: CameraOptions = {
          quality: 100,
          destinationType: this.camera.DestinationType.FILE_URI,
          encodingType: this.camera.EncodingType.JPEG,
          mediaType: this.camera.MediaType.PICTURE,
          correctOrientation: true
      }
      this.camera.getPicture(options).then((imageUri) => 
      {

          if(imageUri != null){
              this.crop.crop(imageUri, {quality: 100})
                  .then(newImage => 
                      {
                          console.log('new image path is: ' + newImage);
                          this.image = newImage;
                          let options = {
                              uri: newImage,
                              quality: 100,
                              width: 200,
                              height: 200
                              } as ImageResizerOptions;
                          this.imageResizer.resize(options).then((data: string) => {
                              console.log('setelah resize' + data);
                              this.image = data;
                          })
                          // .catch(err => console.log(err))
                      }
                      
                  )
                  .catch(error => console.error('Error cropping image', error))
              
          }
      
      })
      .catch((err) => console.log('ga bisa foto coy liat lagi'))
  }

  select()
  {
      let options = {
          maximumImagesCount: 1,
          quality: 100
      }

      this.imagepick.getPictures(options).then((results) => 
      {
          if(results != null){
              for (var i = 0; i < results.length; i++) {
                  this.crop.crop(results[i], {quality: 100})
                  .then(newImage => 
                      {
                          console.log('new image path is: ' + newImage);
                          this.image = newImage;
                          let options = {
                              uri: newImage,
                              quality: 100,
                              width: 200,
                              height: 200
                              } as ImageResizerOptions;
                          this.imageResizer.resize(options).then((data: string) => {
                                  this.image = data;  
                          })
                          // .catch(err => console.log(err))
                      }
                      
                  )
                  .catch(error => console.error('Error cropping image', error))
              
              }
              
              }
    
      },
      (err) => {
          console.log('gagal ambil');
      });
  }

  changeImage(){
      let alert = this.alertCtrl.create({
          title: 'Change Image',
          message: 'How you wish to change your image?',
          buttons: [
          {
              text: 'Choose Image',
              handler: () => { this.select(); }
          },
          {
              text: 'Take Photo',
              handler: () => { this.foto(); }
          },
          {
              text: 'Cancel',
              handler: () => { }
          }
          ],
          enableBackdropDismiss : false
      });
      alert.present();
  }

  sanitizeUrl(url){
    return this.dom.bypassSecurityTrustUrl(url);
  }

  userSaveData(full_name,user_name,description,profile_picture):Promise<any>{
    let headers = new Headers();
    let body =
    { 
        fullname:full_name,
        username:user_name,
        profilePicture:profile_picture,
        description:description,
        sessionToken: this.accountService.getSessionToken()
        //sessionToken:"CNcetBkO7YyKak6rQDjuzkkUt3cFM1qEQhRTP0CijrnNquU3VB44iSKdm7BXmcK2juEqZSCbUHfbANRVa8MTjrI48VWiYm61SqJXsxwSqNQbL7QwTIXBzvVHRvBr1ApobzbvClY8ob8vYrz9gEv20jkR6G0AtKffD0yOGKpTVHMRj5JCTB4V1m1uIYGI1u4iVcN1aZDaiA9X22JCSXmp07P5kr09QToqN4oVYuIl5Fx6d0jWH8Z5orlGZgI01OK"
    };
    console.log(body);
    console.log(JSON.stringify(body));

    headers.append('Content-Type','application/json'); 
    return new Promise(resolve =>
    {
      this .http
        .post(this.networkService.getIp()+"/api/user/update", body,{headers:headers})
        .subscribe((data) => {
            console.log(data.json());
            if(data.json().status_code == 600){
                  resolve(data.json());
            }else{
                  console.log("Error");
                  resolve(data.json());
            }
        })
    });
  }
}
