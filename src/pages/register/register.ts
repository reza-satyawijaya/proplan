import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import {Http, Response, HttpModule, RequestOptions, Headers} from '@angular/http';

import { DashboardPage } from '../dashboard/dashboard';

import { NetworkService } from '../../services/network';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-register',
	templateUrl: 'register.html',
})
export class RegisterPage {
	private fullname: string;
	private username: string;
	private email: string;
	private password: string;
	private confirmationPassword: string;

	homePage: any;
	dashboardPage: any;

	constructor(public navCtrl: NavController, 
				public navParams: NavParams, 
				private toastCtrl: ToastController, 
				private http: Http,
                private loadingCtrl: LoadingController,
                private networkService: NetworkService) {
		this.dashboardPage = DashboardPage;
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad RegisterPage');
	}

	signup() {
		//regex for email
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		if(!re.test(this.email)) {
			const toast = this.toastCtrl.create({
				message: 'Invalid E-mail!',
				duration: 3000,
				position: 'bottom'
			});
			toast.present();
			console.log("Invalid E-mail!");
		}
		else {
			// create loading controller
			let loading = this.loadingCtrl.create({
				content: 'Please wait...'
			});
			loading.present()

			this.http.post(this.networkService.getIp() + '/api/user/create', {
				'fullname': this.fullname, 
				'username': this.username, 
				'email'   :this.email, 
				'password':this.password,
				'confirmationPassword': this.confirmationPassword
			}).subscribe(
				response => {
					let res = JSON.parse(response.text());
					console.log(res);
					if(res.status_code != 600) {
						const toast = this.toastCtrl.create({
						message: res.status_message,
						duration: 3000,
						position: 'bottom'
						});
						toast.present();
					} else {
						const toast = this.toastCtrl.create({
							message: "Sign Up Successful",
							duration: 3000,
							position: 'bottom'
						});
						toast.present();
						this.navCtrl.pop();
					}
					loading.dismiss();
				},
				err => {
					console.log('sam ting wong');
					loading.dismiss();
				}
			);
		}
	}
}
