import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http, Response, HttpModule, RequestOptions, Headers } from '@angular/http';

//Pages
import { ProjectsPage } from '../projects/projects';
import { TeamsPage } from '../teams/teams';

//Services
import { AccountService } from '../../services/account';
import { NetworkService } from '../../services/network';

/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-dashboard',
    templateUrl: 'dashboard.html',
})
export class DashboardPage {
    projectsPage: any;
    teamsPage: any;
    user: any;

    constructor(public navCtrl: NavController, 
                public navParams: NavParams, 
                public accountService: AccountService,
                public networkService: NetworkService,
                public storage: Storage,
                public toastCtrl: ToastController,
                public http: Http) {
        this.projectsPage = ProjectsPage;
        this.teamsPage = TeamsPage;

        // this.storage.get('sessionToken').then((val) => {
        //     if(val != null) {
        //         this.user = val;
        //     } else {
        //         console.log('no session token in sqlite');
        //     }
        // });
        
        // this.accountService.setName(this.navParams.data.username);
        // this.accountService.setSessionToken(this.navParams.data.sessionToken);
        // this.accountService.setId(this.navParams.data.id);
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad DashboardPage');
    }

    moveToProjects(){
        this.navCtrl.push(this.projectsPage,0);
    }
    moveToTeams(){
        this.navCtrl.push(this.teamsPage);
    }
}
