import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';

import {Http, Response, HttpModule, RequestOptions, Headers} from '@angular/http';
import { Subscriber } from 'rxjs/Subscriber';
import { Storage } from '@ionic/storage';

//Pages
import { RegisterPage } from '../register/register';
import { DashboardPage } from '../dashboard/dashboard';
import { ForgotpassPage } from '../forgotpass/forgotpass';

//Services
import { AccountService } from '../../services/account';
import { NetworkService } from '../../services/network';


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-login',
	templateUrl: 'login.html',
})
export class LoginPage {
	private username: string;
	private password: string;
	registerPage: any;
	dashboardPage: any;
	forgotPage: any;
	success: boolean;
	response: Response;
	data: any;

	constructor(public navCtrl: NavController, 
				public navParams: NavParams, 
				private toastCtrl: ToastController, 
				private http: Http, 
                private accSvc: AccountService,
                private networkService: NetworkService,
                private loadingCtrl: LoadingController,
                private storage: Storage) {
		this.registerPage = RegisterPage;
		this.dashboardPage = DashboardPage;
		this.forgotPage = ForgotpassPage;
	}

	ionViewDidLoad() {
	    console.log('ionViewDidLoad LoginPage');
	}

	login(token: string, id: number){
		console.log("Successfully signed in!");
		// pass username, sessionToken, and user id
		// this.navCtrl.setRoot(this.dashboardPage, {
		// 	username: this.username,
		// 	sessionToken: token,
		// 	id: id
        // });
        this.accSvc.setName(this.username);
        this.accSvc.setSessionToken(token);
        this.accSvc.setId(id);

        this.navCtrl.setRoot("DashboardPage");
        //save user sessionToken using storage
        this.storage.set('sessionToken', token);
		this.accSvc.setActive(true);
	}

	signin(){
		if(this.username == "admin"){
		//   this.login();
		}
		else{
			// create loading controller
			let loading = this.loadingCtrl.create({
				content: 'Please wait...'
			});
			loading.present();

			this.http.post(this.networkService.getIp() + '/api/user/login', {
				'username':this.username, 
				'password':this.password
			}).subscribe(
                response => {
                    let res = JSON.parse(response.text());
                    if(res.status_code != 600) {
                        const toast = this.toastCtrl.create({
                            message: 'Wrong Username and/or Password!',
                            duration: 3000,
                            position: 'bottom'
                        });
                        toast.present();
                    } else {
                        this.login(res.result.sessionToken, res.result.id);
                    }
                    loading.dismiss();
                },
                err => {
                    console.log('sam ting wong');
					loading.dismiss();
                }
            );
		}
	}

	signup() {
		this.navCtrl.push(this.registerPage);
	}

	forgot() {
		this.navCtrl.push(this.forgotPage);
	}

}
