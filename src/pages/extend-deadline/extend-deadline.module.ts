import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ExtendDeadlinePage } from './extend-deadline';

@NgModule({
  declarations: [
    ExtendDeadlinePage,
  ],
  imports: [
    IonicPageModule.forChild(ExtendDeadlinePage),
  ],
})
export class ExtendDeadlinePageModule {}
