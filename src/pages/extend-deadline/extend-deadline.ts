import { Component } from '@angular/core';
import { IonicPage, ViewController, NavController, NavParams, ToastController, ModalController, AlertController, LoadingController } from 'ionic-angular';
import { ListPage } from '../list/list';
import { ViewMembersPage } from '../view-members/view-members';

import {Http, Response, HttpModule, RequestOptions, Headers} from '@angular/http';

import { NetworkService } from '../../services/network';
import { AccountService } from '../../services/account';

import { T_Project } from '../../data/project.interface';
import { T_List } from '../../data/list.interface';
import { T_Task } from '../../data/task.interface';
import { T_Checklist } from '../../data/checklist.interface';
import { T_Comment } from '../../data/comment.interface';
import { T_Request } from '../../data/request.interface';
import { T_User } from '../../data/user.interface';
/**
 * Generated class for the ExtendDeadlinePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-extend-deadline',
  templateUrl: 'extend-deadline.html',
})
export class ExtendDeadlinePage {
  requestDate: any;
  extendDate: any;
  leader: any;
  sessionToken: any;
  taskId: any;
  requests: any;
  constructor(public navCtrl: NavController, 
    public toastCtrl: ToastController, 
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public viewCtrl: ViewController,
    public loadingCtrl: LoadingController,
    public http: Http,
    public networkService: NetworkService,
    public accountService: AccountService) {
    this.requestDate = "2018-01-17";
    this.extendDate = "2018-01-17";
    this.taskId = navParams.data.taskId;
    this.leader = false;
    let loading = this.loadingCtrl.create({
      content: 'Fetching data...'
    });
    //loading.present();
    let response = this.http.post(this.networkService.getIp() + '/api/request/isLeader', {
        'sessionToken': this.accountService.getSessionToken(),
        'taskId': this.taskId
    }).subscribe(response => {
        let res = JSON.parse(response.text());
        console.log(res);
        if(res.status_code == 600) {
            //this.leader = res.result;
        }
        //loading.dismiss();
    });
    if(this.isLeader()){
      let loading = this.loadingCtrl.create({
        content: 'Fetching data...'
      });
      //loading.present();
      let response = this.http.post(this.networkService.getIp() + '/api/request/retrieve', {
          'sessionToken': this.accountService.getSessionToken(),
          'taskId': this.taskId
      }).subscribe(response => {
          let res = JSON.parse(response.text());
          console.log(res);
          if(res.status_code == 600) {
              this.requests = [];
              for (let request of res.result) {
                  /*let t_request = new T_Request(request.id, this.taskId, request.dueDate, new T_User(request.user.id, request.user.profilePicture, request.user.fullname, request.user.username, request.user.email, request.user.password, request.user.created_at, request.user.updated_at, request.user.description, request.user.registrationId));
                  this.requests.push(t_request);*/
              }
          }
          //loading.dismiss();
      });
    }
  }
  isLeader(){
    return this.leader;
  }
  requestExtend(){
    let loading = this.loadingCtrl.create({
      content: 'Fetching data...'
    });
    //loading.present();
    console.log(this.requestDate); //GW GATAU INI TIPE DATA APAAN
    let response = this.http.post(this.networkService.getIp() + '/api/request/requestExtend', {
        'sessionToken': this.accountService.getSessionToken(),
        'taskId': this.taskId,
        'dueDate': this.requestDate //GW GATAU INI TIPE DATA APAAN
    }).subscribe(response => {
        let res = JSON.parse(response.text());
        console.log(res);
        if(res.status_code == 600) {
          let toast = this.toastCtrl.create({
              message: "Request sent",
              duration: 2000,
              position: "bottom"
          });
        toast.present();
        }
        //loading.dismiss();
    });
  }
  extendDeadline(){
    let loading = this.loadingCtrl.create({
      content: 'Fetching data...'
    });
    //loading.present();
    console.log(this.extendDate); //GW GATAU INI TIPE DATA APAAN
    let response = this.http.post(this.networkService.getIp() + '/api/request/extendDuedate', {
        'sessionToken': this.accountService.getSessionToken(),
        'taskId': this.taskId,
        'dueDate': this.extendDate //GW GATAU INI TIPE DATA APAAN
    }).subscribe(response => {
        let res = JSON.parse(response.text());
        console.log(res);
        if(res.status_code == 600) {
          let toast = this.toastCtrl.create({
              message: "Extend successful",
              duration: 2000,
              position: "bottom"
          });
        toast.present();
        }
        //loading.dismiss();
    });
  }
  checkAll(){
    let loading = this.loadingCtrl.create({
      content: 'Fetching data...'
    });
    //loading.present();
    let response = this.http.post(this.networkService.getIp() + '/api/request/checkAll', {
        'sessionToken': this.accountService.getSessionToken(),
        'taskId': this.taskId
    }).subscribe(response => {
        let res = JSON.parse(response.text());
        console.log(res);
        if(res.status_code == 600) {
          this.requests = [];
          let toast = this.toastCtrl.create({
              message: "All requests checked",
              duration: 2000,
              position: "bottom"
          });
        toast.present();
        }
        //loading.dismiss();
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ExtendDeadlinePage');
  }
  dismissModal(param: any){
    this.viewCtrl.dismiss(param);
  }
}
