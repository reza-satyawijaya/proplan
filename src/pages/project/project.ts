import { Component } from '@angular/core';
import { IonicPage, ModalController,ActionSheetController, NavController, NavParams, ToastController, AlertController, LoadingController } from 'ionic-angular';
import { ListPage } from '../list/list';
import { ViewMembersPage } from '../../pages/view-members/view-members';
import {Http, Response, HttpModule, RequestOptions, Headers} from '@angular/http';

import { NetworkService } from '../../services/network';
import { AccountService } from '../../services/account';
import { InviteService } from '../../services/invite';

import { T_Project } from '../../data/project.interface';
import { T_List } from '../../data/list.interface';
import { T_Task } from '../../data/task.interface';
import { T_Checklist } from '../../data/checklist.interface';
import { T_Comment } from '../../data/comment.interface';

/**
 * Generated class for the ProjectPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-project',
    templateUrl: 'project.html',
})
export class ProjectPage {
    createlistPage: any;
    listPage: any;
    project: any;
    projectList: T_Project;

    constructor(public navCtrl: NavController, 
                public toastCtrl: ToastController, 
                public navParams: NavParams,
                public actionSheetCtrl: ActionSheetController,
                public alertCtrl: AlertController,
                public loadingCtrl: LoadingController,
                public http: Http,
                public networkService: NetworkService,
                public accountService: AccountService,
                public inviteService : InviteService,
                public modalCtrl:ModalController) {
        this.listPage = ListPage;
        this.project = this.navParams.data.project; //probably will query DB for lists with this params (project ID)
        
        /*let loading = this.loadingCtrl.create({
            content: 'Fetching data...'
        });
        loading.present();

        let response = this.http.post(this.networkService.getIp() + '/api/list/retrieve', {
            'sessionToken': this.accountService.getSessionToken(),
            'projectId': this.project.getId()
        }).subscribe(response => {
            let res = JSON.parse(response.text());
            console.log(res);
            this.project.lists = [];
            for (let list of res.result) {
                let t_list = new T_List(list.id, list.name, list.projectId, list.archived, list.created_at, list.updated_at, new Array<T_Task>());
                for(let task of list.tasks){
                    let newTask = new T_Task(task.id, task.name, list.id, task.description, task.dueDate, task.finished, task.archived, task.created_at, task.updated_at, new Array<T_Checklist>(), new Array<T_Comment>());
                    t_list.tasks.push(newTask);
                }
                this.project.lists.push(t_list);
            }
            loading.dismiss();
        });*/
    }

    reloadProjects () {
        let loading = this.loadingCtrl.create({
            content: 'Fetching data...'
        });
        loading.present();
        this.inviteService.setProject(this.project.getId());
        let response = this.http.post(this.networkService.getIp() + '/api/list/retrieve', {
            'sessionToken': this.accountService.getSessionToken(),
            'projectId': this.project.getId()
        }).subscribe(response => {
            let res = JSON.parse(response.text());
            console.log(res);
            this.project.lists = [];
            for (let list of res.result) {
                let t_list = new T_List(list.id, list.name, list.projectId, list.archived, list.created_at, list.updated_at, new Array<T_Task>());
                for(let task of list.listOfTasks){
                    let newTask = new T_Task(task.id, task.name, list.id, task.description, task.dueDate, task.finished, task.archived, task.created_at, task.updated_at, new Array<T_Checklist>(), new Array<T_Comment>());
                    t_list.tasks.push(newTask);
                }
                this.project.lists.push(t_list);
            }
            loading.dismiss();
        });
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad ProjectPage');
        this.reloadProjects();
    }
    ionViewDidEnter() {
        this.reloadProjects();
    }

    addList() {
        const alert = this.alertCtrl.create({
            title: 'Add New List',
            inputs: [
                {name: 'name', placeholder: 'List Name'}
            ],
            buttons: [
                {
                    text: 'Create',
                    handler: data => {
                        if(data.name.trim() != '' && data.name != null) {
                            let loading = this.loadingCtrl.create({
                                content: 'Please wait...'
                            });
                            loading.present();
                            this.http.post(this.networkService.getIp() + '/api/list/create', {
                                'sessionToken': this.accountService.getSessionToken(),
                                'name': data.name,
                                'projectId': this.project.getId()
                            }).subscribe(response => {
                                let res = JSON.parse(response.text());
                                if(res.status_code == 600) {
                                    var newList = new T_List(res.result.id, data.name, this.project.getId(), res.result.archived, res.result.created_at, res.result.updated_at, new Array<T_Task>());
                                    this.project.lists.push(newList);
                                    console.log(newList);
                                    let toast = this.toastCtrl.create({
                                        message: "List created",
                                        duration: 2000,
                                        position: "bottom"
                                    });
                                    toast.present();
                                }
                                loading.dismiss();
                            });
                            // this.http.post(this.networkService.getIp() + '/api/')
                        }else {
                            let toast = this.toastCtrl.create({
                                message: "Please provide list's name first",
                                duration: 2000,
                                position: "bottom"
                            });
                            toast.present();
                            return false;
                        }
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: data => {
                
                    }
                }
            ]
        });
        alert.present();
        alert.onDidDismiss(() => {
            //oke
        });
    }
  moveToList(l: any){
    this.navCtrl.push(this.listPage,{
      list: l //go to list page carrying the whole list information. probably will only need list ID
    });
  }
  menuList(l: any){
    let actionSheet = this.actionSheetCtrl.create({
        title: 'Edit ' + l.getName(),
        buttons: [
            {
              text: 'Change List Name',
              handler: () => {
                this.updateList(l);
              }
            },
          {
            text: 'Delete List',
            role: 'destructive',
            handler: () => {
              this.deleteList(l);
            }
          },
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              
            }
          }
        ]
      });
   
      actionSheet.present();
  }
  updateList(l: any){
    const alert = this.alertCtrl.create({
        title: 'Edit ' + l.getName() + "'s name",
        inputs: [
            {name: 'name', placeholder: 'New List Name'}
        ],
        buttons: [
            {
                text: 'Edit',
                handler: data => {
                    if(data.name.trim() != '' && data.name != null) {
                        let loading = this.loadingCtrl.create({
                            content: 'Please wait...'
                        });
                        //loading.present();
                        this.http.post(this.networkService.getIp() + '/api/list/update', {
                            'sessionToken': this.accountService.getSessionToken(),
                            'id': l.getId(),
                            'name': data.name
                        }).subscribe(response => {
                            let res = JSON.parse(response.text());
                            console.log(res);
                            if(res.status_code == 600) {
                                let idx = this.project.getLists().indexOf(l);
                                this.project.getLists()[idx].setName(data.name);
                                let toast = this.toastCtrl.create({
                                    message: "List name edited",
                                    duration: 2000,
                                    position: "bottom"
                                });
                                toast.present();
                            }
                            //loading.dismiss();
                        });
                    } else {
                        let toast = this.toastCtrl.create({
                            message: "Please provide list's name first",
                            duration: 2000,
                            position: "bottom"
                        });
                        toast.present();
                        return false;
                    }
                }
            },
            {
                text: 'Cancel',
                role: 'cancel',
                handler: data => {
            
                }
            }
        ]
    });
    alert.present();
    alert.onDidDismiss(() => {
        //oke
    });
  }
  deleteList(l: any){
    let alert = this.alertCtrl.create({
        title: 'Delete ' + l.getName() + '?',
        message: 'Are you sure you want to delete this list?',
        buttons: [
            {
                text: 'Yes',
                handler: () => {
                    let loading = this.loadingCtrl.create({
                        content: 'Please wait...'
                    });
                    loading.present();
                    this.http.post(this.networkService.getIp() + '/api/list/delete', {
                        'sessionToken': this.accountService.getSessionToken(),
                        'id': l.getId()
                    }).subscribe(response => {
                        let res = JSON.parse(response.text());
                        if(res.status_code == 600) {
                            let idx = this.project.lists.indexOf(l);
                            if (idx > -1){
                                this.project.lists.splice(idx, 1);
                            }
                            let toast = this.toastCtrl.create({
                                message: "List deleted",
                                duration: 2000,
                                position: "bottom"
                            });
                            toast.present();
                        }
                        loading.dismiss();
                    });
                }
            },
            {
                text: 'No',
                handler: () => {

                }
            }
        ]
    });
    alert.present();
  }

  goToInvite(){
      this.navCtrl.push("InvitePage");
  }

  goToMember(){
        let profileModal = this.modalCtrl.create("ViewMembersPage", this.project.getId());
        profileModal.present();
      //this.navCtrl.push("ViewMembersPage",this.project.getId());
  }
}
