import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NgForm } from "@angular/forms/src/forms";
import { T_User } from "../../data/user.interface";
import {Http, Response, HttpModule, RequestOptions, Headers} from '@angular/http';
import {AccountService} from '../../services/account';
import {NetworkService} from '../../services/network';
import {InviteService} from '../../services/invite';
import { DomSanitizer } from '@angular/platform-browser';
/**
 * Generated class for the InvitePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-invite',
  templateUrl: 'invite.html',
})
export class InvitePage {
  allMembers: T_User[] = [];
  selectedMember: any = [];
  searchResults: T_User[] = [];
  addedMembers: T_User[] = [];
  userIds:any=[];
  projectId:any=19;
  teamId:any=1;
  constructor(public dom: DomSanitizer,public inviteService:InviteService, public navCtrl: NavController, public navParams: NavParams, public http: Http, public accountService:AccountService, public networkService:NetworkService) {
    var data = {};
    data = this.inviteService.getData();
    this.projectId = data["project"];
    this.teamId = data["team"];
    console.log(data);
    this.userRetrieve().then((data)=>{
      this.allMembers = data["result"];
      console.log(data);
      console.log("SUCCESS");
      console.log(this.allMembers);
    });
  }

  sanitizeUrl(url){
    return this.dom.bypassSecurityTrustUrl(url);
  }

  userRetrieve():Promise<any>{
    let headers = new Headers();
    let body =
    { 
        sessionToken: this.accountService.getSessionToken()
        //sessionToken:"CNcetBkO7YyKak6rQDjuzkkUt3cFM1qEQhRTP0CijrnNquU3VB44iSKdm7BXmcK2juEqZSCbUHfbANRVa8MTjrI48VWiYm61SqJXsxwSqNQbL7QwTIXBzvVHRvBr1ApobzbvClY8ob8vYrz9gEv20jkR6G0AtKffD0yOGKpTVHMRj5JCTB4V1m1uIYGI1u4iVcN1aZDaiA9X22JCSXmp07P5kr09QToqN4oVYuIl5Fx6d0jWH8Z5orlGZgI01OK"
    };

    headers.append('Content-Type','application/json'); 
    return new Promise(resolve =>
    {
      this .http
        .post(this.networkService.getIp()+"/api/user/retrieve", body,{headers:headers})
        .subscribe((data) => {
            console.log(data.json());
            if(data.json().status_message){
                  resolve(data.json());
            }else{
                  resolve(data.json());
            }
        })
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InvitePage');

    //load all members
    // this.allMembers.push(new T_User(1, "https://i.imgur.com/Wk1vkCG.jpg", "Aaaa Aaa", "aaa", "aaaabbbb", "asd", "asd", "asd", "asd", "asd"));
    // this.allMembers.push(new T_User(2, "https://i.imgur.com/Wk1vkCG.jpg", "Bbbb Bbbbbb", "bbb", "abcd", "asd", "asd", "asd", "asd", "asd"));
    // this.allMembers.push(new T_User(3, "https://i.imgur.com/Wk1vkCG.jpg", "Ccc Ccccc", "ccc", "ddfea", "asd", "asd", "asd", "asd", "asd"));
    // this.allMembers.push(new T_User(4, "https://i.imgur.com/Wk1vkCG.jpg", "D Ddd", "ddd", "bba", "asd", "asd", "asd", "asd", "asd"));
    // this.allMembers.push(new T_User(5, "https://i.imgur.com/Wk1vkCG.jpg", "Eeee Eeeeeee", "eee", "abcde", "asd", "asd", "asd", "asd", "asd"));
    // this.allMembers.push(new T_User(6, "https://i.imgur.com/Wk1vkCG.jpg", "Fffff Ff", "fff", "aaabbbcccddd", "asd", "asd", "asd", "asd", "asd"));
    
  }

  search(e, form: NgForm){
    e.preventDefault();
    console.log("Search");
    this.searchResults = [];
    
    var searchQuery = form.value.txtSearch;
    console.log(searchQuery);

    var index, value;
    for (index = 0; index < this.allMembers.length; ++index) {
      // value = this.allMembers[index];
      if (this.allMembers[index].username.includes(searchQuery)) {
        // nemu yang sama kyk input
        // console.log(value);
        this.searchResults.push(this.allMembers[index]);
      }
    }
    console.log(this.searchResults);
  }

  isAdded(member : T_User) : boolean{
    let memberFind: T_User;
    memberFind = this.addedMembers.find((m : T_User, index : number, addedMembers : Array<T_User>) : boolean => {
      return member === m;
    });
    if(memberFind){
      return true;
    } else{
      return false;
    }
  }

  add(member: T_User) {
    console.log(member);
    this.addedMembers.push(member);
    console.log(this.addedMembers);
  }

  remove(member: T_User){
    let index = this.addedMembers.indexOf(member);
    this.addedMembers.splice(index,1);
  }

  isNotEmpty(){
    if(this.addedMembers && this.addedMembers.length){
      //not empty
      return true;
    } else{
      //empty
      return false;
    }
  }

  userProjectInvite():Promise<any>{
    let headers = new Headers();
    let body =
    { 
        sessionToken: this.accountService.getSessionToken(),
        userIds:this.userIds,
        projectId:this.projectId
        //sessionToken:"CNcetBkO7YyKak6rQDjuzkkUt3cFM1qEQhRTP0CijrnNquU3VB44iSKdm7BXmcK2juEqZSCbUHfbANRVa8MTjrI48VWiYm61SqJXsxwSqNQbL7QwTIXBzvVHRvBr1ApobzbvClY8ob8vYrz9gEv20jkR6G0AtKffD0yOGKpTVHMRj5JCTB4V1m1uIYGI1u4iVcN1aZDaiA9X22JCSXmp07P5kr09QToqN4oVYuIl5Fx6d0jWH8Z5orlGZgI01OK"
    };
    console.log("projectinvite");
    console.log(body);
    headers.append('Content-Type','application/json'); 
    return new Promise(resolve =>
    {
      this .http
        .post(this.networkService.getIp()+"/api/invite/inviteToProject", body,{headers:headers})
        .subscribe((data) => {
            console.log(data.json());
            if(data.json().status_message){
                  resolve(data.json());
            }else{
                console.log("Error Invite");
                  resolve(data.json());
            }
        })
    });
  }

  userTeamInvite():Promise<any>{
    let headers = new Headers();
    let body =
    { 
        sessionToken: this.accountService.getSessionToken(),
        userIds:this.userIds,
        teamId:this.teamId
        //sessionToken:"CNcetBkO7YyKak6rQDjuzkkUt3cFM1qEQhRTP0CijrnNquU3VB44iSKdm7BXmcK2juEqZSCbUHfbANRVa8MTjrI48VWiYm61SqJXsxwSqNQbL7QwTIXBzvVHRvBr1ApobzbvClY8ob8vYrz9gEv20jkR6G0AtKffD0yOGKpTVHMRj5JCTB4V1m1uIYGI1u4iVcN1aZDaiA9X22JCSXmp07P5kr09QToqN4oVYuIl5Fx6d0jWH8Z5orlGZgI01OK"
    };
    console.log("teaminvite");
    console.log(body);
    headers.append('Content-Type','application/json'); 
    return new Promise(resolve =>
    {
      this .http
        .post(this.networkService.getIp()+"/api/invite/inviteToTeam", body,{headers:headers})
        .subscribe((data) => {
            console.log(data.json());
            if(data.json().status_message){
                  resolve(data.json());
            }else{
                console.log("Error Invite Team");
                  resolve(data.json());
            }
        })
    });
  }
  inviteMembers(){
    console.log("add member");
    console.log(this.addedMembers);
    for(let a of this.addedMembers){
      this.userIds.push(a.id);
    }
    console.log("hasil id");
    console.log(this.userIds);
    if(this.teamId == 0){
      this.userProjectInvite().then((data)=>{
        console.log("HEHE");
        console.log(data);
        this.navCtrl.push("DashboardPage");
      });
    }
    else{
      this.userTeamInvite().then((data)=>{
        console.log("HAHA");
        console.log(data);
        this.navCtrl.setRoot("DashboardPage");
      });
    }
    this.userIds = [];
  }

  filterItems (ev: any) {
    console.log("asd");
    let val = ev.target.value;
    if (val && val.trim() != '') {
      this.searchResults = this.allMembers.filter((item) => {
        return item.username.toLowerCase().includes(val.toLowerCase());
      });
    } else {
      this.searchResults = [];
    }
  }

}
