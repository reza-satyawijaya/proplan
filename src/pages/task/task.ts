import { Component } from '@angular/core';
import { IonicPage, ActionSheetController, NavController, NavParams, ToastController, ModalController, AlertController, LoadingController } from 'ionic-angular';
import { ListPage } from '../list/list';
import { ExtendDeadlinePage } from '../extend-deadline/extend-deadline';
import { ViewMembersPage } from '../view-members/view-members';

import {Http, Response, HttpModule, RequestOptions, Headers} from '@angular/http';

import { NetworkService } from '../../services/network';
import { AccountService } from '../../services/account';

import { T_Project } from '../../data/project.interface';
import { T_List } from '../../data/list.interface';
import { T_Task } from '../../data/task.interface';
import { T_Checklist } from '../../data/checklist.interface';
import { T_Comment } from '../../data/comment.interface';
import { T_User } from '../../data/user.interface';

import { DomSanitizer } from '@angular/platform-browser';
import * as moment from 'moment';

/**
 * Generated class for the TaskPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-task',
  templateUrl: 'task.html',
})
export class TaskPage {
  task: any;
  inpChecklist: any;
  inpComment: any;
  totalComments: any;
  extendDeadlinePage: any;
  viewMembersPage: any;
  listName: string;
  currentUser: T_User;
  currentProfile: string;
  leader: boolean;
  dateTimeStr: string;

  constructor(public navCtrl: NavController, 
    public toastCtrl: ToastController, 
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public actionSheetCtrl: ActionSheetController,
    public loadingCtrl: LoadingController,
    public http: Http,
    public networkService: NetworkService,
    public accountService: AccountService,
    public dom: DomSanitizer) {

    this.task = this.navParams.data.task; //probably will query DB with the task ID
    this.listName = this.navParams.data.list;
    this.totalComments = 0;
    this.extendDeadlinePage = ExtendDeadlinePage;
    this.viewMembersPage = ViewMembersPage;
    this.leader = false;
    this.currentProfile = "";
    
    /*let loading = this.loadingCtrl.create({
      content: 'Fetching data...'
    });
    let load1Finish = false, load2Finish = false;
    loading.present();
    let responsech = this.http.post(this.networkService.getIp() + '/api/checklist/retrieve', {
        'sessionToken': this.accountService.getSessionToken(),
        'taskId': this.task.getId()
    }).subscribe(responsech => {
        let res = JSON.parse(responsech.text());
        console.log(res);
        if(res.status_code == 600) {
            this.task.checklists = [];
            for (let checklist of res.result) {
                let t_checklist = new T_Checklist(checklist.id, checklist.taskId, checklist.desc, checklist.finished, checklist.created_at, checklist.updated_at);
                this.task.checklists.push(t_checklist);
            }
        }
        load1Finish = true;
        if (load1Finish && load2Finish)
            loading.dismiss();
    });
    /*let loading2 = this.loadingCtrl.create({
      content: 'Fetching data...'
    });
    loading2.present();*/
    /*let responseco = this.http.post(this.networkService.getIp() + '/api/comment/retrieve', {
        'sessionToken': this.accountService.getSessionToken(),
        'taskId': this.task.getId()
    }).subscribe(responseco => {
        let res = JSON.parse(responseco.text());
        console.log(res);
        if(res.status_code == 600) {
            this.task.comments = [];
            for (let comment of res.result) {
                let t_comment = new T_Comment(comment.id, new T_User(comment.user.id, comment.user.profilePicture, comment.user.fullname, comment.user.username, comment.user.email, comment.user.password, comment.user.created_at, comment.user.updated_at, comment.user.description, comment.user.registrationId), comment.taskId, comment.description, comment.created_at, comment.updated_at);
                this.task.comments.push(t_comment);
            }
            this.totalComments = this.task.comments.length;
        }
        load2Finish = true;
        if (load1Finish && load2Finish)
            loading.dismiss();
    });*/
  }
  isLeader(){
    return this.leader;
  }
  reloadTasks () {
    this.userIsLeader();
    this.dateTimeStr = moment(this.task.dueDate).format("D MMM YYYY, mm:ss");
    let loading = this.loadingCtrl.create({
        content: 'Fetching data...'
      });
      let load1Finish = false, load2Finish = false;
      loading.present();
      let responseac = this.http.post(this.networkService.getIp() + '/api/user/get', {
            'sessionToken': this.accountService.getSessionToken()
        }).subscribe(responseac => {
            let res = JSON.parse(responseac.text());
            console.log(res);
            if(res.status_code == 600) {
                this.currentUser = new T_User(
                    res.result.id,
                    res.result.profilePicture,
                    res.result.fullname,
                    res.result.username,
                    res.result.email,
                    "",
                    "",
                    "",
                    res.result.description,
                    res.result.registrationId
                );
                this.currentProfile = this.currentUser.profilePicture;
            }
        });

      let responsech = this.http.post(this.networkService.getIp() + '/api/checklist/retrieve', {
          'sessionToken': this.accountService.getSessionToken(),
          'taskId': this.task.getId()
      }).subscribe(responsech => {
          let res = JSON.parse(responsech.text());
          console.log(res);
          if(res.status_code == 600) {
              this.task.checklists = [];
              for (let checklist of res.result) {
                  let t_checklist = new T_Checklist(checklist.id, checklist.taskId, checklist.description, checklist.finished, checklist.created_at, checklist.updated_at);
                  this.task.checklists.push(t_checklist);
              }
          }
          load1Finish = true;
          if (load1Finish && load2Finish)
              loading.dismiss();
      });
      let responseco = this.http.post(this.networkService.getIp() + '/api/comment/retrieve', {
          'sessionToken': this.accountService.getSessionToken(),
          'taskId': this.task.getId()
      }).subscribe(responseco => {
          let res = JSON.parse(responseco.text());
          console.log(res);
          if(res.status_code == 600) {
              this.task.comments = [];
              for (let comment of res.result) {
                  let t_comment = new T_Comment(comment.id,
                    new T_User(comment.user.id,
                        comment.user.profilePicture,
                        comment.user.fullname,
                        comment.user.username,
                        comment.user.email,
                        comment.user.password,
                        comment.user.created_at,
                        comment.user.updated_at,
                        comment.user.description,
                        comment.user.registrationId),
                    comment.taskId,
                    comment.description,
                    comment.created_at,
                    comment.updated_at);
                  this.task.comments.push(t_comment);
              }
              this.totalComments = this.task.comments.length;
          }
          load2Finish = true;
          if (load1Finish && load2Finish)
              loading.dismiss();
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TaskPage');
    // this.reloadTasks();
  }
  extendDeadline(){
    let modal = this.modalCtrl.create(ExtendDeadlinePage, {
        taskId: this.task.getId()
    });
    modal.onDidDismiss((param1: any) => {
        console.log(param1);
    });
    modal.present();
  }

  viewAllMembers(){
    let modal = this.modalCtrl.create(ViewMembersPage);
    modal.onDidDismiss((param1: any) => {
        console.log(param1);
    });
    modal.present();
  }

  addChecklist(){
    if(this.inpChecklist.trim() != '' && this.inpChecklist != null) {
      let loading = this.loadingCtrl.create({
          content: 'Please wait...'
      });
      loading.present();
      this.http.post(this.networkService.getIp() + '/api/checklist/create', {
          'sessionToken': this.accountService.getSessionToken(),
          'description': this.inpChecklist,
          'taskId': this.task.getId()
      }).subscribe(response => {
          let res = JSON.parse(response.text());
          console.log(res);
          if(res.status_code == 600) {
              var newChecklist = new T_Checklist(res.id, this.task.getId(), this.inpChecklist, res.finished, res.created_at, res.updated_at);
              this.task.checklists.push(newChecklist);
              let toast = this.toastCtrl.create({
                  message: "Checklist created",
                  duration: 2000,
                  position: "bottom"
              });
              toast.present();
          }
          loading.dismiss();
      });
      // this.http.post(this.networkService.getIp() + '/api/')
  }else {
      let toast = this.toastCtrl.create({
          message: "Please provide checklist description",
          duration: 2000,
          position: "bottom"
      });
      toast.present();
      return false;
    }  
  }

  addComment(){
    if(this.inpComment.trim() != '' && this.inpComment != null) {
      let loading = this.loadingCtrl.create({
          content: 'Please wait...'
      });
      loading.present();
      this.http.post(this.networkService.getIp() + '/api/comment/create', {
          'sessionToken': this.accountService.getSessionToken(),
          'description': this.inpComment,
          'taskId': this.task.getId()
      }).subscribe(response => {
          let res = JSON.parse(response.text());
          console.log(res);
          if(res.status_code == 600) {
              var newComment = new T_Comment(res.id, new T_User(this.accountService.getId(), null, null, null, null, null, null, null, null, null), this.task.getId(), this.inpComment, res.created_at, res.updated_at);
              this.task.comments.push(newComment);
              this.totalComments = this.task.comments.length;
              let toast = this.toastCtrl.create({
                  message: "Comment added",
                  duration: 2000,
                  position: "bottom"
              });
              toast.present();
          }
          loading.dismiss();
      });
      // this.http.post(this.networkService.getIp() + '/api/')
  }else {
      let toast = this.toastCtrl.create({
          message: "Please provide comment",
          duration: 2000,
          position: "bottom"
      });
      toast.present();
      return false;
    }  
  }
  goToInvite(){
    this.navCtrl.push("InvitePage");
}

goToMember(){
    let profileModal = this.modalCtrl.create(ViewMembersPage);
        profileModal.present();
    //this.navCtrl.push("ViewMembersPage");
}

  sanitizeUrl (url) {
      return this.dom.bypassSecurityTrustUrl(url);
  }

  dueDateChanged(newDueDate) {
      //api pls
      /*let loading = this.loadingCtrl.create({
        content: 'Fetching data...'
      });
      loading.present();
      let response = this.http.post(this.networkService.getIp() + '/api/request/extend', {
          'sessionToken': this.accountService.getSessionToken(),
          'taskId': this.task.id
      }).subscribe(response => {
          let res = JSON.parse(response.text());
          console.log(res);
          if(res.status_code == 600) {
              this.isLeader = res.result;
          }
          loading.dismiss();
      });*/
  }

  userIsLeader () {
    /*let loading = this.loadingCtrl.create({
        content: 'Fetching data...'
      });*/
      //loading.present();
      console.log("START OF USER");
      let response = this.http.post(this.networkService.getIp() + '/api/request/isLeader', {
          'sessionToken': this.accountService.getSessionToken(),
          'taskId': this.task.id
      }).subscribe(response => {
          let res = JSON.parse(response.text());
          console.log("INI LEADER BUKAN");
          console.log(res);
          if(res.status_code == 600) {
              this.leader = res.result;
          }
          console.log("leader " + this.leader);
          //loading.dismiss();
      });
      console.log("END OF USER");
  }

  updateCheck(ch) {
    let loading = this.loadingCtrl.create({
        content: 'Fetching data...'
      });
      loading.present();
      let response = this.http.post(this.networkService.getIp() + '/api/checklist/finish', {
          'sessionToken': this.accountService.getSessionToken(),
          'id': ch.id
      }).subscribe(response => {
          let res = JSON.parse(response.text());
          console.log(res);
          if(res.status_code == 600) {
            
          }
          loading.dismiss();
      });
  }
  describeTask(){

  }
  menuTask(){
    let actionSheet = this.actionSheetCtrl.create({
        title: 'Edit ' + this.task.getName(),
        buttons: [
            {
              text: 'Change Task Name',
              handler: () => {
                this.renameTask();
              }
            },
            {
              text: 'Edit Description',
              handler: () => {
                this.describeTask();
              }
            },
          {
            text: 'Delete Task',
            role: 'destructive',
            handler: () => {
              this.deleteTask();
            }
          },
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              
            }
          }
        ]
      });
   
      actionSheet.present();
}
renameTask(){
    const alert = this.alertCtrl.create({
        title: 'Edit ' + this.task.getName() + "'s name",
        inputs: [
            {name: 'name', placeholder: 'New Project Name'}
        ],
        buttons: [
            {
                text: 'Edit',
                handler: data => {
                    if(data.name.trim() != '' && data.name != null) {
                        let loading = this.loadingCtrl.create({
                            content: 'Please wait...'
                        });
                        //loading.present();
                        this.http.post(this.networkService.getIp() + '/api/task/update', {
                            'sessionToken': this.accountService.getSessionToken(),
                            'id': this.task.getId(),
                            'name': data.name
                        }).subscribe(response => {
                            let res = JSON.parse(response.text());
                            console.log(res);
                            if(res.status_code == 600) {
                                this.task.setName(data.name);
                                let toast = this.toastCtrl.create({
                                    message: "Task name edited",
                                    duration: 2000,
                                    position: "bottom"
                                });
                                toast.present();
                            }
                            //loading.dismiss();
                        });
                    } else {
                        let toast = this.toastCtrl.create({
                            message: "Please provide task's name first",
                            duration: 2000,
                            position: "bottom"
                        });
                        toast.present();
                        return false;
                    }
                }
            },
            {
                text: 'Cancel',
                role: 'cancel',
                handler: data => {
            
                }
            }
        ]
    });
    alert.present();
    alert.onDidDismiss(() => {
        //oke
    });
}

//blom dibuat api-nya
deleteTask(){
    let alert = this.alertCtrl.create({
        title: 'Delete ' + this.task.name + '?',
        message: 'Are you sure you want to delete this task?',
        buttons: [
            {
                text: 'Yes',
                handler: () => {
                    let loading = this.loadingCtrl.create({
                        content: 'Please wait...'
                    });
                    //loading.present();
                    this.http.post(this.networkService.getIp() + '/api/task/delete', {
                        'sessionToken': this.accountService.getSessionToken(),
                        'id': this.task.getId()
                    }).subscribe(response => {
                        let res = JSON.parse(response.text());
                        console.log(res);
                        if(res.status_code == 600) {
                            let toast = this.toastCtrl.create({
                                message: "Task deleted",
                                duration: 2000,
                                position: "bottom"
                            });
                            toast.present();
                            this.navCtrl.pop();
                        }
                        loading.dismiss();
                    });
                }
            },
            {
                text: 'No',
                handler: () => {

                }
            }
        ]
    });
    alert.present();
}
}
