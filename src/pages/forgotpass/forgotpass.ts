import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import {Http, Response, HttpModule, RequestOptions, Headers} from '@angular/http';

/**
 * Generated class for the ForgotpassPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forgotpass',
  templateUrl: 'forgotpass.html',
})
export class ForgotpassPage {
  private email: string;
  constructor(public navCtrl: NavController, public navParams: NavParams, private toastCtrl: ToastController, private http: Http) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotpassPage');
  }

  forgotpw(){
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(!re.test(this.email)) {
      const toast = this.toastCtrl.create({
        message: 'Invalid E-mail!',
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
      console.log("Invalid E-mail!");
    }
    else {
      const toast = this.toastCtrl.create({
        message: 'E-mail has been sent to ' + this.email + '!',
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
      console.log("E-mail has been sent!");

      this.navCtrl.pop();
    }
  }

}
