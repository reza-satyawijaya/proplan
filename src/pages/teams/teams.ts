import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { TeamPage } from '../team/team';
import { AccountService } from '../../services/account';
import { NetworkService } from '../../services/network';
import { Http, Headers, Response }      from '@angular/http';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
/**
 * Generated class for the TeamsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-teams',
  templateUrl: 'teams.html',
})
export class TeamsPage {
  teamPage: any;
  team:any = [];
  createteamPage: any;
  user: any;
  constructor(public navCtrl: NavController, private alertCtrl:AlertController, private http: Http, public navParams: NavParams, private accountService: AccountService, private networkService: NetworkService, private loadingCtrl:LoadingController) {
    this.teamPage = TeamPage;
    let loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `Please Wait`
    });
    loading.present();
    this.teamData().then((data)=>{
      console.log("Isi Data Team");
      console.log(data);
      this.team = data["result"];
      loading.dismiss();
    });
    this.user = this.navParams.data.user; //string containing the user's name only
  }

  ionViewCanEnter(){
    let loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `Please Wait`
    });
    loading.present();
    this.teamData().then((data)=>{
      console.log("Isi Data Team");
      console.log(data);
      this.team = data["result"];
      loading.dismiss();
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TeamsPage');
  }
  // moveToTeam(t: String){
  //   this.navCtrl.push(this.teamPage, {
  //     team: t
  //   });
  // }

  moveToTeam(dataTeam:any){
    var object = {};
    object = dataTeam;
    console.log(object);
    this.navCtrl.push(this.teamPage,object);
  }

  // moveToCreateteam(){
  //   // this.navCtrl.push(this.createteamPage, {
  //   //   user: this.user
  //   // });

  // }

  moveToCreateTeam(){
    let alert = this.alertCtrl.create({
      title: 'Create A New Team',
      inputs: [
        {
          name: 'name',
          placeholder: "Team's Name",
          type : 'text'
        },
        {
          name: 'description',
          placeholder: 'Description',
          type: 'textarea'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Create',
          handler: data => {
            if (data.name != "" && data.description != "" ) {
              this.createTeamAPI(data.name,data.description).then((data)=>{
                console.log(data);
                console.log("Success buat team");
              });
            } else {
              console.log("Salah Brow");
              return false;
            }
          }
        }
      ]
    });
    alert.present();
  }

  createTeamAPI(names,descriptions):Promise<any>{
    let headers = new Headers();
    let body =
    { 
        sessionToken: this.accountService.getSessionToken(),
        name:names,
        description:descriptions
        //sessionToken:"CNcetBkO7YyKak6rQDjuzkkUt3cFM1qEQhRTP0CijrnNquU3VB44iSKdm7BXmcK2juEqZSCbUHfbANRVa8MTjrI48VWiYm61SqJXsxwSqNQbL7QwTIXBzvVHRvBr1ApobzbvClY8ob8vYrz9gEv20jkR6G0AtKffD0yOGKpTVHMRj5JCTB4V1m1uIYGI1u4iVcN1aZDaiA9X22JCSXmp07P5kr09QToqN4oVYuIl5Fx6d0jWH8Z5orlGZgI01OK"
    };
    console.log(body);
    console.log(JSON.stringify(body));

    headers.append('Content-Type','application/json'); 
    return new Promise(resolve =>
    {
      this .http
        .post(this.networkService.getIp()+"/api/team/create", body,{headers:headers})
        .subscribe((data) => {
            console.log(data.json());
            if(data.json().status_code == "600"){
                  resolve(data.json());
            }else{
                console.log("Error");
                  resolve(data.json());
            }
        })
    });
  }

  teamData():Promise<any>{
    let headers = new Headers();
    let body =
    { 
        sessionToken: this.accountService.getSessionToken()
        //sessionToken:"CNcetBkO7YyKak6rQDjuzkkUt3cFM1qEQhRTP0CijrnNquU3VB44iSKdm7BXmcK2juEqZSCbUHfbANRVa8MTjrI48VWiYm61SqJXsxwSqNQbL7QwTIXBzvVHRvBr1ApobzbvClY8ob8vYrz9gEv20jkR6G0AtKffD0yOGKpTVHMRj5JCTB4V1m1uIYGI1u4iVcN1aZDaiA9X22JCSXmp07P5kr09QToqN4oVYuIl5Fx6d0jWH8Z5orlGZgI01OK"
    };
    console.log(body);
    console.log(JSON.stringify(body));

    headers.append('Content-Type','application/json'); 
    return new Promise(resolve =>
    {
      this .http
        .post(this.networkService.getIp()+"/api/team/retrieve", body,{headers:headers})
        .subscribe((data) => {
            console.log(data.json());
            if(data.json().status_code == "600"){
                  resolve(data.json());
            }else{
                  console.log("Error Team");
                  resolve(data.json());
            }
        })
    });
  }
}
