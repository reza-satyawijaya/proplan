import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { ProjectsPage } from '../projects/projects';
import { AccountService } from '../../services/account';
import { NetworkService } from '../../services/network';
import { InviteService } from '../../services/invite';
import { Http, Headers, Response }      from '@angular/http';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { ImageResizer, ImageResizerOptions } from '@ionic-native/image-resizer';
import { Crop } from '@ionic-native/crop';
import { Base64 } from '@ionic-native/base64';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ImagePicker } from '@ionic-native/image-picker';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
/**
 * Generated class for the TeamPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-team',
  templateUrl: 'team.html',
})
export class TeamPage {
  projectsPage: any;
  teamId:any;
  teamData:any=[];
  teamTitle: any;
  team:FormGroup;
  image:any;
  constructor(public navCtrl: NavController,
              private dom: DomSanitizer, 
              private inviteService : InviteService,
              private http: Http, 
              public formBuilder:FormBuilder,
              public navParams: NavParams, 
              private accountService: AccountService, 
              private networkService: NetworkService, 
              private loadingCtrl:LoadingController,
              private camera        : Camera,
              private imagepick     : ImagePicker,
              private imageResizer  : ImageResizer,
              private crop          : Crop,
              private base64        : Base64,
              private alertCtrl     : AlertController,
              private toastCtrl     : ToastController) {
    this.projectsPage = ProjectsPage;
    this.teamTitle = this.navParams.data.name;
    let loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `Please Wait`
    });
    loading.present();
    this.teamDetails(this.navParams.data.id).then((data)=>{
      console.log("teamdetails");
      console.log(data);
      this.teamData = data;
      this.inviteService.setTeam(this.teamData["result"]["team"]["id"]);
      this.team = this.formBuilder.group({
        nameTeam: [this.teamData["result"]["team"]["name"], Validators.required],
        descriptionTeam: [this.teamData["result"]["team"]["description"]]
      });
      //this.image="https://pm1.narvii.com/6411/3bd9805238c8268113654c09aaf4355ffc97a971_hq.jpg";
      this.image = this.teamData["result"]["team"]["profilePicture"];
      loading.dismiss();
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TeamPage');
  }

  moveToProjects(){
    this.navCtrl.push(this.projectsPage,this.navParams.data.id);
  }

  updateTeamProfile(){
    let toast = this.toastCtrl.create({
        message: 'Update Team Succesfully',
        duration: 3000,
        position: 'bottom'
      });
    
      toast.onDidDismiss(() => {
        console.log('Dismissed toast');
      });
    
      
    this.base64.encodeFile(this.image)
    .then(data_image => 
      {
      this.teamUpdate(this.navParams.data.id,this.team.value.nameTeam,this.team.value.descriptionTeam,data_image).then(()=>{
        console.log("Berhasil update team");
        toast.present();
        this.navCtrl.setRoot("DashboardPage");
      });
    });
  }

  foto()
  {
      const options: CameraOptions = {
          quality: 100,
          destinationType: this.camera.DestinationType.FILE_URI,
          encodingType: this.camera.EncodingType.JPEG,
          mediaType: this.camera.MediaType.PICTURE,
          correctOrientation: true
      }
      this.camera.getPicture(options).then((imageUri) => 
      {

          if(imageUri != null){
              this.crop.crop(imageUri, {quality: 100})
                  .then(newImage => 
                      {
                          console.log('new image path is: ' + newImage);
                          this.image = newImage;
                          let options = {
                              uri: newImage,
                              quality: 100,
                              width: 200,
                              height: 200
                              } as ImageResizerOptions;
                          this.imageResizer.resize(options).then((data: string) => {
                              console.log('setelah resize' + data);
                              this.image = data;
                          })
                          // .catch(err => console.log(err))
                      }
                      
                  )
                  .catch(error => console.error('Error cropping image', error))
              
          }
      
      })
      .catch((err) => console.log('ga bisa foto coy liat lagi'))
  }

  select()
  {
      let options = {
          maximumImagesCount: 1,
          quality: 100
      }

      this.imagepick.getPictures(options).then((results) => 
      {
          if(results != null){
              for (var i = 0; i < results.length; i++) {
                  this.crop.crop(results[i], {quality: 100})
                  .then(newImage => 
                      {
                          console.log('new image path is: ' + newImage);
                          this.image = newImage;
                          let options = {
                              uri: newImage,
                              quality: 100,
                              width: 200,
                              height: 200
                              } as ImageResizerOptions;
                          this.imageResizer.resize(options).then((data: string) => {
                                  this.image = data;  
                          })
                          // .catch(err => console.log(err))
                      }
                      
                  )
                  .catch(error => console.error('Error cropping image', error))
              
              }
              
              }
    
      },
      (err) => {
          console.log('gagal ambil');
      });
  }

  changeImage(){
      let alert = this.alertCtrl.create({
          title: 'Change Image',
          message: 'How you wish to change your image?',
          buttons: [
          {
              text: 'Choose Image',
              handler: () => { this.select(); }
          },
          {
              text: 'Take Photo',
              handler: () => { this.foto(); }
          },
          {
              text: 'Cancel',
              handler: () => { }
          }
          ],
          enableBackdropDismiss : false
      });
      alert.present();
  }

  teamDetails(teamId):Promise<any>{
    let headers = new Headers();
    let body =
    { 
        sessionToken: this.accountService.getSessionToken(),
        id:teamId
        //sessionToken:"CNcetBkO7YyKak6rQDjuzkkUt3cFM1qEQhRTP0CijrnNquU3VB44iSKdm7BXmcK2juEqZSCbUHfbANRVa8MTjrI48VWiYm61SqJXsxwSqNQbL7QwTIXBzvVHRvBr1ApobzbvClY8ob8vYrz9gEv20jkR6G0AtKffD0yOGKpTVHMRj5JCTB4V1m1uIYGI1u4iVcN1aZDaiA9X22JCSXmp07P5kr09QToqN4oVYuIl5Fx6d0jWH8Z5orlGZgI01OK"
    };
    console.log(body);
    console.log(JSON.stringify(body));

    headers.append('Content-Type','application/json'); 
    return new Promise(resolve =>
    {
      this .http
        .post(this.networkService.getIp()+"/api/team/get", body,{headers:headers})
        .subscribe((data) => {
            console.log(data.json());
            if(data.json().status_code == "600"){
                  resolve(data.json());
            }else{
                  console.log("Error Team");
                  resolve(data.json());
            }
        })
    });
  }


  teamUpdate(teamId,names,descriptions,profiles):Promise<any>{
    let headers = new Headers();
    let body =
    { 
        sessionToken: this.accountService.getSessionToken(),
        id:teamId,
        name:names,
        description:descriptions,
        profilePicture:profiles
        //sessionToken:"CNcetBkO7YyKak6rQDjuzkkUt3cFM1qEQhRTP0CijrnNquU3VB44iSKdm7BXmcK2juEqZSCbUHfbANRVa8MTjrI48VWiYm61SqJXsxwSqNQbL7QwTIXBzvVHRvBr1ApobzbvClY8ob8vYrz9gEv20jkR6G0AtKffD0yOGKpTVHMRj5JCTB4V1m1uIYGI1u4iVcN1aZDaiA9X22JCSXmp07P5kr09QToqN4oVYuIl5Fx6d0jWH8Z5orlGZgI01OK"
    };
    console.log(body);
    console.log(JSON.stringify(body));

    headers.append('Content-Type','application/json'); 
    return new Promise(resolve =>
    {
      this .http
        .post(this.networkService.getIp()+"/api/team/update", body,{headers:headers})
        .subscribe((data) => {
            console.log(data.json());
            if(data.json().status_code == "600"){
                  resolve(data.json());
            }else{
                  console.log("Error Team");
                  resolve(data.json());
            }
        })
    });
  }
  goToInvite(){
    this.navCtrl.push("InvitePage");
  }


  sanitizeUrl(url){
    return this.dom.bypassSecurityTrustUrl(url);
  }
}
