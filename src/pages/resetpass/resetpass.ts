import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import {Http, Response, HttpModule, RequestOptions, Headers} from '@angular/http';

/**
 * Generated class for the ResetpassPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-resetpass',
  templateUrl: 'resetpass.html',
})
export class ResetpassPage {
  private newpass: string;
  private confirmpass: string;
  constructor(public navCtrl: NavController, public navParams: NavParams, private toastCtrl: ToastController, private http: Http) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResetpassPage');
  }

  resetpw(){
    const toast = this.toastCtrl.create({
      message: 'Password has been changed!',
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
    console.log("Password has been changed!");

    this.navCtrl.pop();
  }

}
