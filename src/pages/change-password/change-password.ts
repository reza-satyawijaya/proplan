import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Http, Headers, Response }      from '@angular/http';
import { NetworkService } from '../../services/network';
import { AccountService } from '../../services/account';
/**
 * Generated class for the ChangePasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-change-password',
  templateUrl: 'change-password.html',
})
export class ChangePasswordPage {

  private changePassword : FormGroup;
  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public formBuilder:FormBuilder, 
              public http:Http,
              public networkService:NetworkService,
              public accountService:AccountService,
              public toastCtrl: ToastController) {
    this.changePassword = this.formBuilder.group({
      oldPassword: ["", Validators.required],
      newPassword: ["", Validators.required],
      confirmPassword: ["", Validators.required],
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangePasswordPage');
  }

  changePasswordForm(){
    this.userChangePassword(this.changePassword.value["oldPassword"],this.changePassword.value["newPassword"],this.changePassword.value["confirmPassword"]).then(()=>{
      console.log("Success Change Password");
    });
  }

  userChangePassword(oldPasswords,newPasswords,confirmPasswords):Promise<any>{
    let headers = new Headers();
    let body =
    { 
        sessionToken: this.accountService.getSessionToken(),
        oldPassword:oldPasswords,
        newPassword:newPasswords,
        confirmationPassword:confirmPasswords
        //sessionToken:"CNcetBkO7YyKak6rQDjuzkkUt3cFM1qEQhRTP0CijrnNquU3VB44iSKdm7BXmcK2juEqZSCbUHfbANRVa8MTjrI48VWiYm61SqJXsxwSqNQbL7QwTIXBzvVHRvBr1ApobzbvClY8ob8vYrz9gEv20jkR6G0AtKffD0yOGKpTVHMRj5JCTB4V1m1uIYGI1u4iVcN1aZDaiA9X22JCSXmp07P5kr09QToqN4oVYuIl5Fx6d0jWH8Z5orlGZgI01OK"
    };
    console.log(body);
    console.log(JSON.stringify(body));

    headers.append('Content-Type','application/json'); 
    return new Promise(resolve =>
    {
      this .http
        .post(this.networkService.getIp()+"/api/user/changePassword", body,{headers:headers})
        .subscribe((data) => {
            console.log(data.json());
            if(data.json().status_code == "600"){
                  resolve(data.json());
                  let toast = this.toastCtrl.create({
                    message: 'Change Password succesfully',
                    duration: 3000,
                    position: 'top'
                  });
                
                  toast.onDidDismiss(() => {
                    console.log('Dismissed toast');
                  });
                
                  toast.present();
            }else{
                  console.log("Error Change Password");
                  resolve(data.json());
            }
        })
    });
  }

}
