import { Component } from '@angular/core';
import { IonicPage, ActionSheetController,ModalController, NavController, NavParams, ToastController, AlertController, LoadingController } from 'ionic-angular';
import { TaskPage } from '../task/task';

import {Http, Response, HttpModule, RequestOptions, Headers} from '@angular/http';

import { NetworkService } from '../../services/network';
import { AccountService } from '../../services/account';
import { InviteService } from '../../services/invite';
import { ViewMembersPage } from '../view-members/view-members';
import { T_Project } from '../../data/project.interface';
import { T_List } from '../../data/list.interface';
import { T_Task } from '../../data/task.interface';
import { T_Checklist } from '../../data/checklist.interface';
import { T_Comment } from '../../data/comment.interface';
import { T_User } from '../../data/user.interface';

/**
 * Generated class for the ListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-list',
  templateUrl: 'list.html',
})
export class ListPage {
  taskPage: any;
  list: any;
  taskList: T_List;

  finishedTaskCount: number;
  totalTaskCount: number;

  constructor(public navCtrl: NavController, 
    public toastCtrl: ToastController, 
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public actionSheetCtrl: ActionSheetController,
    public http: Http,
    public networkService: NetworkService,
    public accountService: AccountService,
    public inviteService:InviteService,
    public modalCtrl:ModalController) {
    this.taskPage = TaskPage;
    this.list = this.navParams.data.list; //probably will query DB with list ID as key
    
    /*let loading = this.loadingCtrl.create({
      content: 'Fetching data...'
    });
    loading.present();
    let response = this.http.post(this.networkService.getIp() + '/api/task/retrieve', {
        'sessionToken': this.accountService.getSessionToken(),
        'listId': this.list.getId()
    }).subscribe(response => {
      let res = JSON.parse(response.text());
      this.list.tasks = [];
      for (let task of res.result) {
          let t_task = new T_Task(
              task.id,
              task.name,
              task.listId,
              task.description,
              task.dueDate,
              task.finished,
              task.archived,
              task.created_at,
              task.updated_at,
              new Array<T_Checklist>(),
              new Array<T_Comment>());
          this.list.tasks.push(t_task);
      }
      loading.dismiss();
    });*/
  }

  reloadLists () {
    let loading = this.loadingCtrl.create({
        content: 'Fetching data...'
      });
      loading.present();
      let response = this.http.post(this.networkService.getIp() + '/api/task/retrieve', {
          'sessionToken': this.accountService.getSessionToken(),
          'listId': this.list.getId()
      }).subscribe(response => {
        let res = JSON.parse(response.text());
        this.list.tasks = [];
        for (let task of res.result) {
            let t_task = new T_Task(
                task.id,
                task.name,
                task.listId,
                task.description,
                task.dueDate,
                task.finished,
                task.archived,
                task.created_at,
                task.updated_at,
                new Array<T_Checklist>(),
                new Array<T_Comment>());

            let responsech = this.http.post(this.networkService.getIp() + '/api/checklist/retrieve', {
                'sessionToken': this.accountService.getSessionToken(),
                'taskId': t_task.getId()
            }).subscribe(responsech => {
                let res = JSON.parse(responsech.text());
                console.log(res);
                if(res.status_code == 600) {
                    //task.checklists = [];
                    for (let checklist of res.result) {
                        let t_checklist = new T_Checklist(checklist.id, checklist.taskId, checklist.description, checklist.finished, checklist.created_at, checklist.updated_at);
                        t_task.insertChecklist(t_checklist);
                    }
                }
            });
            let responseco = this.http.post(this.networkService.getIp() + '/api/comment/retrieve', {
                'sessionToken': this.accountService.getSessionToken(),
                'taskId': t_task.getId()
            }).subscribe(responseco => {
                let res = JSON.parse(responseco.text());
                console.log(res);
                if(res.status_code == 600) {
                    //task.comments = [];
                    for (let comment of res.result) {
                        let t_comment = new T_Comment(comment.id, new T_User(comment.user.id, comment.user.profilePicture, comment.user.fullname, comment.user.username, comment.user.email, comment.user.password, comment.user.created_at, comment.user.updated_at, comment.user.description, comment.user.registrationId), comment.taskId, comment.description, comment.created_at, comment.updated_at);
                        t_task.insertComment(t_comment);
                    }
                }
            });
            this.list.tasks.push(t_task);
        }
        loading.dismiss();
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListPage');
    this.reloadLists();
  }
  ionViewDidEnter() {
      this.reloadLists();
  }
  moveToTask(t: any){
    this.navCtrl.push(this.taskPage, {
      task: t, //go to task page passing the whole info. probably only need task ID
      list: this.list.name //for list name hahaha bruteforce
    });
  }
  addTask() {
    const alert = this.alertCtrl.create({
        title: 'Add New Task',
        inputs: [
            {name: 'name', placeholder: 'Task Name'}
        ],
        buttons: [
            {
                text: 'Create',
                handler: data => {
                    if(data.name.trim() != '' && data.name != null) {
                        let loading = this.loadingCtrl.create({
                            content: 'Please wait...'
                        });
                        loading.present();
                        this.http.post(this.networkService.getIp() + '/api/task/create', {
                            'sessionToken': this.accountService.getSessionToken(),
                            'name': data.name,
                            'listId': this.list.getId()
                        }).subscribe(response => {
                            let res = JSON.parse(response.text());
                            console.log(res);
                            if(res.status_code == 600) {
                                var newTask = new T_Task(res.result.id, data.name, this.list.getId(), res.result.description, res.result.dueDate, res.result.finished, res.result.archived, res.result.created_at, res.result.updated_at, new Array<T_Checklist>(), new Array<T_Comment>());
                                this.list.tasks.push(newTask);
                                console.log(newTask);
                                let toast = this.toastCtrl.create({
                                    message: "List created",
                                    duration: 2000,
                                    position: "bottom"
                                });
                                toast.present();
                            }
                            loading.dismiss();
                        });
                        // this.http.post(this.networkService.getIp() + '/api/')
                    }else {
                        let toast = this.toastCtrl.create({
                            message: "Please provide task's name first",
                            duration: 2000,
                            position: "bottom"
                        });
                        toast.present();
                        return false;
                    }
                }
            },
            {
                text: 'Cancel',
                role: 'cancel',
                handler: data => {
            
                }
            }
        ]
    });
    alert.present();
    alert.onDidDismiss(() => {
        //oke
    });
}
    goToInvite(){
        this.navCtrl.push("InvitePage");
    }

goToMember(){
    let profileModal = this.modalCtrl.create(ViewMembersPage);
        profileModal.present();
    //this.navCtrl.push("ViewMembersPage");
}
    menuTask(t: any){
        let actionSheet = this.actionSheetCtrl.create({
            title: 'Edit ' + t.getName(),
            buttons: [
                {
                  text: 'Change Task Name',
                  handler: () => {
                    this.renameTask(t);
                  }
                },
              {
                text: 'Delete Task',
                role: 'destructive',
                handler: () => {
                  this.deleteTask(t);
                }
              },
              {
                text: 'Cancel',
                role: 'cancel',
                handler: () => {
                  
                }
              }
            ]
          });
       
          actionSheet.present();
    }
    renameTask(t: any){
        const alert = this.alertCtrl.create({
            title: 'Edit ' + t.getName() + "'s name",
            inputs: [
                {name: 'name', placeholder: 'New Task Name'}
            ],
            buttons: [
                {
                    text: 'Edit',
                    handler: data => {
                        if(data.name.trim() != '' && data.name != null) {
                            let loading = this.loadingCtrl.create({
                                content: 'Please wait...'
                            });
                            loading.present();
                            this.http.post(this.networkService.getIp() + '/api/task/update', {
                                'sessionToken': this.accountService.getSessionToken(),
                                'id': t.getId(),
                                'name': data.name
                            }).subscribe(response => {
                                let res = JSON.parse(response.text());
                                if(res.status_code == 600) {
                                    let idx = this.list.tasks.indexOf(t);
                                    this.list.tasks[idx].setName(data.name);
                                    let toast = this.toastCtrl.create({
                                        message: "Task name edited",
                                        duration: 2000,
                                        position: "bottom"
                                    });
                                    toast.present();
                                }
                                loading.dismiss();
                            });
                        } else {
                            let toast = this.toastCtrl.create({
                                message: "Please provide task's name first",
                                duration: 2000,
                                position: "bottom"
                            });
                            toast.present();
                            return false;
                        }
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: data => {
                
                    }
                }
            ]
        });
        alert.present();
        alert.onDidDismiss(() => {
            //oke
        });
    }

    //blom dibuat api-nya
    deleteTask(t: any){
        let alert = this.alertCtrl.create({
            title: 'Delete ' + t.name + '?',
            message: 'Are you sure you want to delete this task?',
            buttons: [
                {
                    text: 'Yes',
                    handler: () => {
                        let loading = this.loadingCtrl.create({
                            content: 'Please wait...'
                        });
                        loading.present();
                        this.http.post(this.networkService.getIp() + '/api/task/delete', {
                            'sessionToken': this.accountService.getSessionToken(),
                            'id': t.getId()
                        }).subscribe(response => {
                            let res = JSON.parse(response.text());
                            if(res.status_code == 600) {
                                let idx = this.list.tasks.indexOf(t);
                                if (idx > -1){
                                    this.list.tasks.splice(idx, 1);
                                }
                                let toast = this.toastCtrl.create({
                                    message: "Task deleted",
                                    duration: 2000,
                                    position: "bottom"
                                });
                                toast.present();
                            }
                            loading.dismiss();
                        });
                    }
                },
                {
                    text: 'No',
                    handler: () => {

                    }
                }
            ]
        });
        alert.present();
    }
}
