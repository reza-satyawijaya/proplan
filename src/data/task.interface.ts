import { T_Checklist } from '../data/checklist.interface';
import { T_Comment } from '../data/comment.interface';

export class T_Task {
    id: number;
    name: string;
    listId: number;
    desc: string;
    dueDate: string;
    finished: boolean;
    archived: boolean;
    created_at: boolean;
    updated_at: boolean;
    checklists: T_Checklist[];
    comments: T_Comment[];

    finishedTaskCount: number;
    totalTaskCount: number;
    commentCount: number;

    constructor (id: number, name: string, listId: number, desc: string, dueDate: string, finished: boolean, archived: boolean, created_at: boolean, updated_at: boolean, checklists: T_Checklist[], comments: T_Comment[]) {
        this.id = id;
        this.name = name;
        this.listId = listId;
        this.desc = desc;
        this.dueDate = dueDate;
        this.finished = finished;
        this.archived = archived;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.checklists = checklists;
        this.comments = comments;
        this.totalTaskCount = this.checklists.length;
        this.finishedTaskCount = this.getFinishedCheckCount();
        this.commentCount = this.comments.length;
    }
    getId(){
        return this.id;
    }
    getName(){
        return this.name;
    }
    private getFinishedCheckCount () {
        let finishedChecks = 0;
        for (let check of this.checklists) {
            if (check.finished) {
                finishedChecks++;
            }
        }
        return finishedChecks;
    }

    insertChecklist (newCheck: T_Checklist) {
        this.checklists.push(newCheck);
        this.totalTaskCount = this.checklists.length;
        this.finishedTaskCount = this.getFinishedCheckCount();
    }

    insertComment (newComment: T_Comment) {
        this.comments.push(newComment);
        this.commentCount = this.comments.length;
    }

    setName(name: string) {
        this.name = name;
    }
}