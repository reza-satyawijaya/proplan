import { T_User } from '../data/user.interface';

export class T_Comment {
    id: number;
    user: T_User;
    taskId: number;
    desc: string;
    created_at: string;
    updated_at: string;

    constructor (id: number, user: T_User, taskId: number, desc: string, created_at: string, updated_at: string){
        this.id = id;
        this.user = user;
        this.taskId = taskId;
        this.desc = desc;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }
}