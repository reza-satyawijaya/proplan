export class T_User {
    id: number;
    profilePicture: string;
    fullname: string;
    username: string;
    email: string;
    password: string;
    created_at: string;
    updated_at: string;
    description: string;
    registrationId: string;

    constructor (id: number,
        profilePicture: string,
        fullname: string,
        username: string,
        email: string,
        password: string,
        created_at: string,
        updated_at: string,
        description: string,
        registrationId: string){
            this.id = id;
            this.profilePicture = profilePicture;
            this.fullname = fullname;
            this.username = username;
            this.email = email;
            this.password = password;
            this.created_at = created_at;
            this.updated_at = updated_at;
            this.description = description;
            this.registrationId = registrationId;
    }
}