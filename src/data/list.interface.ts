import { T_Task } from '../data/task.interface';

export class T_List {
    id: number;
    name: string;
    projectId: number;
    archived: boolean;
    createdAt: string;
    updatedAt: string;
    tasks: T_Task[];

    constructor (id: number, name: string, projectId: number, archived: boolean, createdAt: string, updatedAt: string, tasks: T_Task[]) {
        this.id = id;
        this.name = name;
        this.projectId = projectId;
        this.archived = archived;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.tasks = tasks.slice();
    }
    getId(){
        return this.id;
    }
    getName(){
        return this.name;
    }
    setName(name: string){
        this.name = name;
    }
}