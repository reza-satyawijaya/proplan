import { T_User } from '../data/user.interface';

export class T_Request {
    id: number;
    taskId: number;
    dueDate: string;
    user: T_User;

    constructor (id: number,
        taskId: number,
        dueDate: string,
        user: T_User){
            this.id = id;
            this.user = user;
            this.taskId = taskId;
            this.dueDate = dueDate;
    }
}