export class T_Checklist {
    id: number;
    taskId: number;
    desc: string;
    finished: boolean;
    created_at: string;
    updated_at: string;

    constructor (id: number, taskId: number, desc: string, finished: boolean, created_at: string, updated_at: string){
        this.id = id;
        this.taskId = taskId;
        this.desc = desc;
        this.finished = finished;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    setFinish() {
        if(this.finished == true) this.finished = false;
        else this.finished = true;
    }
}