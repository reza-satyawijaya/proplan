import { T_List } from '../data/list.interface';

export class T_Project {
    id: number;
    name: string;
    ownerId: number;
    ownerName: string;
    star: boolean;
    lists: T_List[];

    constructor (id: number, name: string, ownerId: number, ownerName: string, star: boolean, lists: T_List[]) {
        this.id = id;
        this.name = name;
        this.ownerId = ownerId;
        this.ownerName = ownerName;
        this.star = star;
        this.lists = lists.slice();
    }
    getId(){
        return this.id;
    }
    getName(){
        return this.name;
    }
    getStar(){
        return this.star;
    }
    getLists(){
        return this.lists.slice();
    }
    setName(name: string){
        this.name = name;
    }
    setStar(star: boolean){
        this.star = star;
    }
    toggleStar(){
        if (this.star == true) this.star = false;
        else if (this.star == false) this.star = true;
    }
}