import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import { Camera } from '@ionic-native/camera';
import { ImagePicker } from '@ionic-native/image-picker';
import { ImageResizer } from '@ionic-native/image-resizer';
import { Crop } from '@ionic-native/crop';
import { Base64 } from '@ionic-native/base64';
import { MyApp } from './app.component';
import { OneSignal } from '@ionic-native/onesignal';

//Pages
import { ProjectsPage } from '../pages/projects/projects';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { TeamsPage } from '../pages/teams/teams';
import { ProjectPage } from '../pages/project/project';
import { TeamPage } from '../pages/team/team';
<<<<<<< HEAD
=======
//import { CreateteamPage } from '../pages/createteam/createteam';
>>>>>>> fb051a6b7308f1573688bb479e77efa365179288
import { ListPage } from '../pages/list/list';
import { TaskPage } from '../pages/task/task';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { ForgotpassPage } from '../pages/forgotpass/forgotpass';
import { ResetpassPage } from '../pages/resetpass/resetpass';
import { ViewMembersPage } from "../pages/view-members/view-members";
import { ExtendDeadlinePage } from "../pages/extend-deadline/extend-deadline";

//Services
import { AccountService } from '../services/account';
import { NetworkService } from '../services/network';
import { InviteService } from '../services/invite';
import { MomentModule } from 'angular2-moment';

@NgModule({
	declarations: [
		MyApp,
		ProjectsPage,
		//DashboardPage,
		TeamsPage,
		ProjectPage,
		TeamPage,
		//CreateteamPage,
		ListPage,
		TaskPage,
		LoginPage,
		RegisterPage,
		//InvitePage,
		ForgotpassPage,
		ResetpassPage,
		ExtendDeadlinePage,
		//ViewMembersPage
	],
	imports: [
		BrowserModule,
		IonicModule.forRoot(MyApp),
        HttpModule,
		IonicStorageModule.forRoot(),
		MomentModule
	],
	bootstrap: [IonicApp],
	entryComponents: [
		MyApp,
		ProjectsPage,
		//DashboardPage,
		TeamsPage,
		ProjectPage,
		TeamPage,
		//CreateteamPage,
		ListPage,
		TaskPage,
		LoginPage,
		RegisterPage,
		//InvitePage,
		ForgotpassPage,
		ResetpassPage,
		ExtendDeadlinePage,
		//ViewMembersPage
	],
	providers: [
		StatusBar,
		SplashScreen,
		AccountService,
		NetworkService,
		InviteService,
		Camera,
		ImagePicker,
		ImageResizer,
		Crop,
		Base64,
		{provide: ErrorHandler, useClass: IonicErrorHandler},	
        OneSignal
	]
})
export class AppModule {}