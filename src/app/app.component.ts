import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, MenuController, ModalController, LoadingController, ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';

import { Http, Response, HttpModule, RequestOptions, Headers } from '@angular/http';
import { OneSignal } from '@ionic-native/onesignal';

//Pages
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { InvitePage } from '../pages/invite/invite';
import { DashboardPage } from '../pages/dashboard/dashboard';

//Services
import { AccountService } from '../services/account';
import { NetworkService } from '../services/network';

//Operators for angular 2 observable
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/map';

@Component({
	templateUrl: 'app.html'
})
export class MyApp {
    signedIn: boolean;
    rootPage:any = LoginPage;
    registerPage:any = RegisterPage;
    dashboardPage:any = DashboardPage;

    @ViewChild('sideMenuContent') nav: NavController;

    constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,
                private menuCtrl: MenuController,
                private modalCtrl: ModalController,
                private accSvc: AccountService,
                private networkService: NetworkService,
                private http: Http,
                private oneSignal: OneSignal,
                private storage: Storage,
                private loadingCtrl: LoadingController,
                private toastCtrl: ToastController) {
        platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();

        });
        this.signedIn = this.accSvc.isLoggedIn();

        //get sessionToken from storage, if exist, direct to dashboard immediately
        this.storage.get('sessionToken').then((token) => {
            if(token != null) {
                console.log('Your session token is ', token);

                // retrieve username and id by using sessionToken
                // create loading controller
                let loading = this.loadingCtrl.create({
                    content: 'Please wait...'
                });
                loading.present();

                this.http.post(this.networkService.getIp() + "/api/user/get", {
                    'sessionToken': token
                })
                .subscribe(
                    response => {
                        let res = JSON.parse(response.text());
                        if(res.status_code != 600) {
                            const toast = this.toastCtrl.create({
                                message: 'Failed to login, check storage',
                                duration: 3000,
                                position: 'bottom'
                            });
                            toast.present();
                        } else {
                            this.signedIn = true;
                            // this.nav.setRoot(this.dashboardPage, {
                            //     username: res.result.username,
                            //     sessionToken: token,
                            //     id: res.result.id
                            // });
                            
                            //onesignal
                            if(platform.is("core")) {
                                this.oneSignal.startInit('e39c2f6f-e893-4917-912c-82f92740db83', '463608752399');
                                this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
                                this.oneSignal.setSubscription(true);
                                this.oneSignal.getIds().then(ids => {
                                    console.log(JSON.stringify(ids['userId']));
                                    this.http.post(this.networkService.getIp() + "/api/user/registrate", {
                                        sessionToken: token,
                                        registrationId: ids['userId']
                                    }).subscribe((response)=>{
                                        console.log(ids['userId']);
                                        console.log("Hasil registrate");
                                        console.log(response);
                                        // this.pushNotif().then((data)=>{
                                        //     console.log("Berhasil");
                                        //     console.log(data);
                                        // })
                                    });
                                    this.oneSignal.handleNotificationReceived().subscribe(() => {
                                        const toast = this.toastCtrl.create({
                                            message: 'Notification received',
                                            duration: 3000,
                                            position: 'bottom'
                                        });
                                        
                                        this.oneSignal.handleNotificationOpened().subscribe(() => {
                                            const toast = this.toastCtrl.create({
                                                message: 'Opened from notification',
                                                duration: 3000,
                                                position: 'bottom'
                                            });
                                            toast.present();
                                        });
                                        this.oneSignal.endInit();
            
                                    });   
                                });
                            }
                            
                            
                            this.accSvc.setName(res.result.username);
                            this.accSvc.setSessionToken(token);
                            this.accSvc.setId(res.result.id);

                            this.nav.setRoot("DashboardPage");
                        }
                        loading.dismiss();
                    },
                    err => {
                        const toast = this.toastCtrl.create({
                            message: 'sam ting wong',
                            duration: 3000,
                            position: 'bottom'
                        });
                        toast.present();
					    loading.dismiss();
                    }
                );
            } else {
                console.log('No username stored');
                this.nav.setRoot(LoginPage);
            }
        });
        console.log(this.signedIn);
    }

    pushNotif():Promise<any>{
        let headers = new Headers();
        let body =
        { 
            app_id: 'e39c2f6f-e893-4917-912c-82f92740db83',
            contents: {"en": "HEHEHEHE"},
            include_player_ids: ["2d852500-1024-44ab-ade5-1a3d3e8ee30a"]
            //sessionToken:"CNcetBkO7YyKak6rQDjuzkkUt3cFM1qEQhRTP0CijrnNquU3VB44iSKdm7BXmcK2juEqZSCbUHfbANRVa8MTjrI48VWiYm61SqJXsxwSqNQbL7QwTIXBzvVHRvBr1ApobzbvClY8ob8vYrz9gEv20jkR6G0AtKffD0yOGKpTVHMRj5JCTB4V1m1uIYGI1u4iVcN1aZDaiA9X22JCSXmp07P5kr09QToqN4oVYuIl5Fx6d0jWH8Z5orlGZgI01OK"
        };
        console.log(body);
        console.log(JSON.stringify(body));
    
        headers.append('Content-Type','application/json');
        headers.append('Authorization','Basic NGEwMGZmMjItY2NkNy0xMWUzLTk5ZDUtMDAwYzI5NDBlNjJj'); 
        return new Promise(resolve =>
        {
          this .http
            .post('https://onesignal.com/api/v1/notifications', body,{headers:headers})
            .subscribe((data) => {
                console.log(data.json());
                if(data.json().status_message){
                      resolve(data.json());
                }else{
                    console.log("Error");
                      resolve(data.json());
                }
            })
        });
    }

    onLoad(page:any) {
        this.nav.setRoot(page);
        this.menuCtrl.close();
    }

    isThisActive(){
        if(this.signedIn == false) {
            this.signedIn = this.accSvc.isLoggedIn();
        }
        return this.signedIn;
    }

    signup() {
        this.nav.push(this.registerPage);
        this.menuCtrl.close();
    }

    //side menu function
    inviteNewMember() {
        this.nav.push(InvitePage);
    }

    logout() {
        this.accSvc.setActive(false);
        this.nav.setRoot(this.rootPage);
        //remove sessionToken from storage
        this.storage.remove('sessionToken');
        this.signedIn = false;
        this.menuCtrl.close();
    }

    goToProfilePage(){
        this.nav.push("ProfilePage");
    }

    goToChangePasswordPage(){
        this.nav.push("ChangePasswordPage");
    }
}

