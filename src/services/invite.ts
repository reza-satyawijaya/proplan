export class InviteService {
    private inviteData:any = false;
    private memberData:any = false;
    private projectId:any =0;
    private teamId:any =0;
    setInvite(){
        this.inviteData = !this.inviteData;
    }
    getInvite(){
        return this.inviteData;
    }
    setMember(){
        this.memberData = !this.memberData;
    }
    getMember(){
        return this.memberData;
    }
    setTeam(team:any){
        this.projectId = 0;
        this.teamId = team;
    }
    setProject(project:any){
        this.projectId = project;
        this.teamId = 0;
    }
    getData(){
        var object ={};
        object["project"] = this.projectId;
        object["team"] = this.teamId;
        this.projectId = 0;
        this.teamId = 0;
        return object;
    }
}   