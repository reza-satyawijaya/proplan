export class AccountService {
    private isActive = false;
    public sessionToken : string;
    private name: string;
    public id: number;

    setActive (isLogin: boolean) {
        this.isActive = isLogin;
    }
    isLoggedIn () {
        return this.isActive;
    }

    setSessionToken(sessionToken: string) {
        this.sessionToken = sessionToken;
    }
    getSessionToken() {
        return this.sessionToken;
    }

    setName(name: string) {
        this.name = name;
    }
    getName() {
        return this.name;
    }

    setId(id: number) {
        this.id = id;
    }
    getId() {
        return this.id;
    }
}   